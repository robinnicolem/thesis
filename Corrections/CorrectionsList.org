

* The rest of the thesis                                          :no_export:
** TODO *Suggestions* put the $(A_1, A_2)$ in baricentric coordinates (triangle) in which would give a better intuition of the dynamics
** TODO Be sure that in the figure all the data are there to reproduce the data provided
** TODO Maybe add a word about the Kuramoto model 
** TODO Change the $\mathbb{P}$ which is usually used to denote probability of events while here we have a *pdf*. e.g. Eq. 2.44, 2.12 
** TODO Check the numerics for the Fig. 2.3
** TODO Make the caption of Fig. 3.4 clearer
Especially explain that the dashed lines are there to explain the intuition
** TODO Sec. 4.2: there is two memory lengths r = 0.01 and r = 0.05 which one is the good one? 
** TODO Generally in *Chap. 4* Explain keep one vocabulary either slow or fast traders (see e.g. Fig. 4.2)
** TODO In the comments on Fig. 4.1 the values of $\beta_{\rm c}$ are not consistent with the curves on the figure.
* Specific to the part on the population of ideas                 :no_export:
** TODO Modify the introduction to include more the Chap. 5 in the thesis. 
Adding the 
** TODO Change notations as well $\to$ make a notation table 
*** $\Gamma \to \beta$ this is the equivalent of an intensity of choice in the former chapters :no_export:
*** $Q \to A$ and actions being the calligraphic letters ${\cal A, B}$ to connect those to the attractions in the rest of the paper
*** To homogenize the notation one could also change the label of the payoff but I think it would be confusing especially because it is the notation used in the litterature 
