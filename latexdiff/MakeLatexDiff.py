#!/usr/bin/python
import os
import re


def MakeLatexDiff(RepDirectory='/home/k1327990/these/',
                  GITLOG='8c5bee2c483bb6471b21319acb547971fc98bf13',
                  FilePath='02_Chapters/Intro/Introduction.tex',
                  OldFile=False,
                  OutputPrefix='Diff',
                  StartPreamble='/Users/robin/Thesis/' +
                  'latexdiff/preambleStart.tex',
                  EndPreamble='/Users/robin/Thesis/' +
                  'latexdiff/preambleEnd.tex'):
    '''
    Return None.
    @param RepDirectory: Directory of the Master latex file.
    @param GITLOG: commit hash with which we want
    latexdiff the current file with.
    @param OldFile: if set to False will run latexdiff against
    the former git commit indicated by GITLOG otherwise will
    run git commit against the file in the path indicated by
    OldFile
    @param OutputName: Name of the outputfile
    '''

    OutputName = OutputPrefix + '.tex'
    cwd = os.getcwd()
    os.chdir(RepDirectory)  # Change to the main directory
    # Getting the filename
    File = FilePath.split('/')[-1]
    File = File.split('.tex')[0]  # extract the suffic before .tex
    # if the old file is not specified use git to check the older version
    if OldFile is False:
        CommandToRun = 'git show ' + GITLOG + ':' + FilePath +\
                       ' > ' + RepDirectory + 'latexdiff/' + File + 'Old.tex'
    else:
        CommandToRun = 'cp ' + OldFile + ' ' + RepDirectory + \
                       'latexdiff/' + File + 'Old.tex'
    # Copy the former file
    os.system(CommandToRun)
    CopyCommand = 'cp ' + FilePath + ' ' + 'latexdiff/'
    os.system(CopyCommand)
    # add preamble to the files and put the result in the main directory
    os.chdir('latexdiff')
    os.system('cat ' + StartPreamble + ' ' + File +
              'Old.tex ' + EndPreamble + ' > ../' +
              OutputPrefix + 'OlDiff.tex')
    os.system('cat ' + StartPreamble + ' ' + File +
              '.tex ' + EndPreamble + ' > ../' + OutputPrefix + 'Newiff.tex')
    # change to the main directory
    os.chdir('../')
    # Run latexdiff
    os.system('latexdiff ' + OutputPrefix + 'OlDiff.tex ' +
              OutputPrefix + 'Newiff.tex > ' + OutputPrefix + 'Diff.tex')
    os.system('pdflatex -shell-escape -interaction=nonstopmode ' +
              OutputPrefix + 'Diff.tex')
    os.system('pdflatex -shell-escape -interaction=nonstopmode ' +
              OutputPrefix + 'Diff.tex')
    os.system('pdflatex -shell-escape -interaction=nonstopmode ' +
              OutputPrefix + 'Diff.tex')
    os.system('pdflatex -shell-escape -interaction=nonstopmode ' +
              OutputPrefix + 'Diff.tex')
    os.system('cp ' + OutputPrefix + 'Diff.pdf ' + OutputName)
    #print(([OutputPrefix + 'OldDiff.tex',  OutputPrefix + 'Newiff.tex'])
    os.chdir(cwd)


import argparse

parser = argparse.ArgumentParser()
### Positional
parser.add_argument('--RepDirectory', 
                    default=os.getcwd(),
                    help='Directory of the thesis')
args = parser.parse_args()
print(args.RepDirectory)


## 
def LDiffPopOfIdeasThesis():
    MakeLatexDiff(GITLOG='6fc9310b005b91eceeff88886f3ee0287da6dac2',
                  FilePath='02_Chapters/PopulationOfIdeas/PopulationOfIdeasMainText.tex',
                  OutputPrefix='PopIdeas',
                  StartPreamble="/home/k1327990/these/latexdiff/preambleStart.tex",
                  EndPreamble='/home/k1327990/these/latexdiff/preambleEnd.tex')

def LDiffWholeThesis():
    FileList = [("02_Chapters/PopulationOfIdeas/PopulationOfIdeasMainText.tex","PopIdeas"),
                ("02_Chapters/DynamicalSelection/Main.tex", "DynSelec"),
                ("02_Chapters/Hetergeneous_memory/Main.tex", "HetMem"),
                ("02_Chapters/ThreeMarkets/Main.tex","3Markets"),
                ("02_Chapters/Intro/Introduction.tex","Intro")]
    
    for f, name in FileList:
        MakeLatexDiff(GITLOG='6fc9310b005b91eceeff88886f3ee0287da6dac2',
                      FilePath=f,
                      OutputPrefix=name,
                      StartPreamble="/home/k1327990/these/latexdiff/preambleStart.tex",
                      EndPreamble='/home/k1327990/these/latexdiff/preambleEnd.tex')
        os.system("rm ../"+ name +"Diff.aux")
        os.system("rm ../"+ name +"Diff.out")
        os.system("rm ../"+ name +"Diff.tex")
        os.system("rm ../"+ name +"OlDiff.tex")
        os.system("rm ../"+ name +"Diff.log")
        os.system("rm ../"+ name +"Newiff.tex")
        os.system("rm ../"+ name +".tex")

        
