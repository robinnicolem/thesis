\chapter{Varying memory and decision strength}
\label{chap:HetMem}
\section{Introduction}
\label{sec:HetMem:introduction}
In Chap.~\ref{chap:Dynamic} and~\ref{chap:3M} we made the assumption that the traders discounted the payoffs of past trades at the same rate $r$.\myMarginnote{Context}
Although this assumption made our analysis clearer, it ignores the diversity of traders' objectives.
In practice, traders have access to different pieces of information, have different expectations, and can have different interpretations of shared information that is widely available. 
For example, the time scale over which \emph{investors} expect to make a profit is much larger than the timescale over which \emph{speculators} expect to make a profit.
This chapter aims to present a simple extension of the model defined in Chap.~\ref{chap:Dynamic} which takes into account heterogeneity in the behaviour of traders. 

The consequences of heterogeneity in the information received by traders is a widely discussed topic.\myMarginnote{bibliography}
% One can, for example, cite the serie of articles from Grossman \etal{}~[] where they discuss how price in markets transmit information from informed to uninformed traders.
For example, in Ref.~\cite{tothscalas}, subtitled ``The luck of the uninformed'', Toth \etal{} study the dynamics of traders with different information levels who learn to trade in a double auction market.
Surprisingly in such settings, the profit of traders doesn't always increase with their information level.
Bloembergen \etal{} explain in~\cite{Bloembergen2015}: ``One possible theory explaining this phenomenon is that more information helps during trends, whereas limited knowledge may be erroneous when the trend reverses; uninformed traders are safe from these systematic mistakes''.
The study of the dynamics of traders with heterogeneous memory playing the minority game has also attracted attention from physicists~\cite{WOHLMUTH2006459,Johnson1999}. For example,~\cite{Johnson1999} reports enhanced winnings for traders with long memory in a population where the memory length of players is heterogeneous.
Our aim is to investigate the robustness of segregation to the introduction of heterogeneities in both the memory and the intensity of choice of traders. 

This chapter is organised as follows: in Sec.~\ref{het:sec:theory} we introduce the theoretical formalism we use in order to study the co-learning between fast and slow traders.
In Sec.~\ref{het:sec:results} we apply the theory developed in Sec.~\ref{het:sec:theory} to investigate the influence of heterogeneity of traders on the phenomenon of segregation.


\section{Dynamics of the slow and fast traders}
\label{het:sec:theory}
In the examples we consider in the next section, part of the population has an inverse memory length $r \ll 1$ and another part of the population has an inverse memory length $r = 1$.
A quick look at the EWA learning update rules (see Eq.~\eqref{eq:Dyn:EWA_dynamics}), shows that to change its preference toward a market by a quantity of order $O(1)$, a trader with inverse memory length $r = 0.01$ will need $O(1/r) = O(100)$ rounds while a trader with finite memory will only need $O(1)$ rounds. 
This timescale separation between the dynamics of those two types of traders is the reason why we choose to call the traders with an inverse memory $r = 1$ \emph{fast traders} and the traders with an inverse memory $r = 0.01$ \emph{slow traders}.
As a consequence of this timescale separation, the slow traders will be seen as static by the fast traders, which will simplify our analysis.
The market mechanisms and the learning dynamics of the traders are \emph{exactly} the same as in Chap.~\ref{chap:Dynamic}; the fictitious play parameter $\alpha$ is set to $1$ throughout this chapter.
This choice simplifies the equations obtained from the \KM{} expansion for the dynamics of the slow traders.
To study the dynamics of fast and slow traders, we will need to use two different methods.
On the one hand, the natural way to describe the dynamics of slow traders is to perform a \KM{} expansion similar to the one we did in Apps.~\ref{app:Dyn:kmexp}, Sec.~\ref{sec:3M:km} and Sec.~\ref{sec:kram-moyal-expans}.
On the other hand, the memory length of fast traders is not small $r = 1$ and this rules out using a \KM{} expansion (which relies on the smallness of $r$) to derive a Fokker-Planck equation for the dynamics of fast traders. Fortunately the short memory case $r=1$ can be analysed directly because the traders' preferences become instantaneously determined by their payoffs, as we now show. 

\subsection{Update equations for fast traders}
When looking at the expression of the preferences update rules in Eq.~\eqref{eq:Dyn:EWA_dynamics} for fast traders ($r = 1$) who use fictitious play ($\alpha = 1$), one sees that they become:

\begin{equation}
  \Afbfs{m}(n+1) =\left\{
    \begin{array}{cc}
       \payoff(n) & \text{if the trader chose market $m$ in round $n$}\\
      0 & \text{otherwise}                                                      
    \end{array}\right.  
\label{eq:hetmem:EWA_dynamics}
\end{equation}

Here $\payoff(n)$ is the score the trader earned at time $n$  and traders use the logit rule described in Eq.~\eqref{eq:Dyn:ptrade} to choose the next market they will trade with. In the rest of this subsection, we will derive the probability to trade at the first market step by step.
First, from the description of the trading process in Sec.~\ref{sec:Dyn:model} we can calculate the probability density function of the profit ${\cal S}$ of a trader who places an order to buy ($\tau = {\rm a}$) or sell ($\tau= {\rm b}$) at market $m$ is:

\begin{align}\myMarginnote{distribution of the score}
  \label{eq:probascores}
  \mathbb{P}(S \mid \tau, M = m) =& \mathcal{V}(\tau,m) \mathcal{M}(\tau,m,f_m) \frac{1}{\sqrt{2 \pi} \sigma}\exp(-\frac{(S - \pi_\tau)^2}{2 \sigma^2}) \theta(S)\notag\\
  &+ \delta(S) \left(1 - \mathcal{V}(\tau,m)\mathcal{M}(\tau,m,f_m)\right) 
\end{align}
Therefore, for a trader that has preference to buy $\pb$, the probability to get a profit $S$ when trading at market $m$ is:
\begin{equation}
  %\label{eq:1}
  \mathbb{P}(S \mid M =  m) = \pb \mathbb{P}(S \mid {\rm b}, M = m) + (1 - \pb) \mathbb{P}(S \mid {\rm a}, M = m) 
\end{equation}
In the expressions above, $\mathcal{V}(\tau,m)$ and $\mathcal{M}(\mathrm{a},m,f_m)$ are respectively the probability to send a valid order and the probability for the validated order to be matched so that a trade can take place (see Appendix~\ref{app:Dyn:PayoffFormula}). Also $\theta(\cdot)$ is the Heaviside function which is $0$ if its argument is negative and $1$ otherwise and $\delta(\cdot)$ is the Dirac $\delta$-function.
Let us consider $\mathbb{P}(M = m, n \mid M = m,n-1, f_m, \pb)$ which is ``the probability that a trader with preferences to buy $\pb$ goes to market $m$ at time step $t$, knowing that he went to the same market $m$ at time step $n-1$ and that the ratio of buyers over sellers in this market was $f_m$''. For readability we shorten this by $\mathbb{P}(m \to m, f_m, \pb)$: 
\begin{align}
  \myMarginnote{transition probability}
  \mathbb{P}(m \to m, f_m, \pb) =& \int  {\rm d}S\, \mathbb{P} (M = m, n \mid M = m , n-1,S ) \mathbb{P}(S \mid M = m, \pb)
  \notag\\
  =& \pb \int_0^\infty \mathcal{V}({\rm b},m) \mathcal{M}({\rm b},m,f_m) \frac{1}{\sqrt{2 \pi} \sigma}\frac{\exp(-\frac{(S - \pi_b)^2}{2 \sigma^2})}{1 + \exp(-\beta S)}\notag\\
                                 &+ (1-\pb) \int_0^\infty \mathcal{V}({\rm a},m) \mathcal{M}({\rm a},m,f_m) \frac{1}{\sqrt{2 \pi} \sigma}\frac{\exp(-\frac{(S - \pi_{\rm a})^2}{2 \sigma^2})}{1 + \exp(-\beta S)}
\end{align}
%\todo[inline]{Shall I keep the second set of equations?}
Since after having traded at market $m$, the preferences of a trader are given by Eq.~\eqref{eq:hetmem:EWA_dynamics}, we deduce that the probability for a trader to trade at market $m$ knowing that s/he previously traded at market $m$ and achieved profit $S$ is given by $\mathbb{P} (M = m, n \mid M = m , n-1,S ) =\frac{1}{1 + \exp(-\beta S)}$.
If we denote by $m^\dag$ the opposite market to $m$ (\ie{} $m^\dag = 1$ if $m = 2$ and $m^\dag = 2$ if $ m =1$) then the probability to go from market $m$ at time step $n-1$ to market $m^\dag$ at time step $n$ is:
\begin{align}
  \mathbb{P}(m \to m^\dag, f_{m}, \pb) = 1 - \mathbb{P} (m \to m, f_m, \pb)
\end{align}
We then deduce from the law of large numbers that in the large population limit, $\bar{p}^{(c)}(n)$, the fraction of fast traders from class $c$ going to market $1$ at time step $n$ can be calculated from the following recursive equation:
\begin{align}\myMarginnote{update equation}
  \bar{p}^{(c)}(n) &= \mathbb{P}(1 \to 1, f_1, \pb^{(c)})\bar{p}^{(c)}(n-1) + \mathbb{P}(2 \to 1, f_2, \pb^{(c)}) (1-\bar{p}^{(c)}(n-1))%\\
  % \mathbb{P}(2,t) &= \mathbb{P}(1 \to 2, f_1, \pb)\mathbb{P}(1,t-1) + \mathbb{P}(2 \to 2, f_2, \pb) \mathbb{P}(2, t-1)
                      \label{eq:het:fast_traders}
\end{align}
This equation still involves the ratio of buyers over sellers in market $m$, $f_m$, which depends on the distribution of both fast and slow traders across both markets.

\subsection{Distribution of the scores of the slow traders}
\label{sec:het:distslow}
When one performs a \KM{} expansion  of the slow traders' master equation truncated at the second order, one sees that the dynamics of the distribution of difference $\Delta A = A_1 - A_2$  between the scores at market 1 and market 2 is:
\begin{align}\myMarginnote{KM expansion for slow traders}
     \partial_t \mathbb{P}(\Delta A^{(c)},t) &= -\partial_{\Delta A^{(c)}}\left[\mu^{(c)}(\Delta A^{(c)},f_1, f_2)\mathbb{P}(\Delta A^{(c)},t)\right]\notag\\
                                             &{}+\frac{r}{2} \partial_{\Delta A^{(c)}} \partial_{\Delta A^{(c)}} \left[\Sigma^{(c)}(\Delta A^{(c)},f_1, f_2)\mathbb{P}(\Delta A^{(c)},t)\right]
                                                \label{eq:het:kmexp_slow}
\end{align}
where the expressions of $\mu^{(c)}(\Delta A^{(c)},f_1, f_2)$ and $\Sigma^{(c)}(\Delta A^{(c)},f_1, f_2)$ are given in Appendix~\ref{app:het:kmexp}. Note that the dynamics of the slow and fast traders are coupled by the values of the aggregates $f_1$ and $f_2$.
Moreover, it will be useful for the rest of this section to note that the stationary solution of the Fokker-Planck equation~\eqref{eq:het:kmexp_slow} has a closed form expression:
\begin{align}\myMarginnote{score distribution in the steady state of the learning dynamics}
  \mathbb{P}(\Delta A^{(c)}) \propto  \frac{1}{\Sigma^{(c)}(\Delta A^{(c)},f_1, f_2)} \exp\left(\frac{2}{r} \int_0^y \frac{\mu^{(c)}(y,f_1, f_2)}{\Sigma^{(c)}(y,f_1, f_2)}{\rm d}y\right)
  \label{eq:het:closedform}
\end{align}
where the sign $\propto$ means that the equation above is true up to a normalization constant.
With the equation for the dynamics of both fast traders and slow traders \eqref{eq:het:fast_traders},\eqref{eq:het:kmexp_slow} it only remains to find the aggregates $f_1$ and $f_2$ to calculate the distribution of scores in the whole population. To find the value of the aggregates, we use an iterative approach. We start with an initial guess for the aggregate from which we calculate the number of fast and slow traders in each markets with the total number of slow (resp.\ fast) traders being constant.
We use those results to get a new estimate of the market conditions (using some damping to ensure convergence). We stop the procedure when the market conditions have converged.
 
% We first initialize $N_{\text{slow}}^{(c)}$ the number of traders from class $c \in \{1,2\}$ going to market $1$ and follow the steps 1 to 3 described below as it was done by Alori\'c in~\cite{thesisaleksandra}: @@ again, refer to somewhere in your thesis if possible - you also use analogous self-consistency arguments elsewhere? @@
% \begin{itemize}
% \item[\textbf{step 1}] calculate the steady state of the fast traders equation with the procedure detailed in appendix~\ref{app:het:fastfp} and thus determine $N_{\text{fast}}^{(c)}$, the number of fast traders from class $c$ in market $1$.
%   \myMarginnote{fixed point algorithm}
% \item[\textbf{step 2}] calculate the stationary solution of Eq.~\eqref{eq:het:kmexp_slow} at fixed aggregates $\tilde{f}_1, \tilde{f}_2$ which are deduced from the number of slow ($N_{\text{slow}}^{(c)}$) and fast traders---calculated in step 2---in market one. Calculate the number of fast and slow traders from each class in market $1$ from this distribution: $\tilde{N}_{\text{slow}}^{(c)}$
% \item[\textbf{step 3}] update the number of slow traders $N_{\text{slow}}^{(c)} = (1-a) N_{\text{slow}}^{(c)} + a \tilde{N}_{\text{slow}}^{(c)}$
% \end{itemize}
% The quadruplet $N_{\text{fast}}^{(1)},N_{\text{fast}}^{(2)}, N_{\text{slow}}^{(1)},N_{\text{slow}}^{(2)}$ can then be used to calculate the score distribution of fast traders with Eq.~\eqref{eq:het:fast_traders} and slow traders with Eq.~\eqref{eq:het:closedform}.
% A is a damping coefficient to ensure the convergence of the fixed point algorithm. 
% @@ comments on the above: you should say what $a$ is (damping coefficient for updates?) Also you should mention that there is a fixed overall number of fast and slow traders in each class, and clarify where this feeds into the iterative flxed point calculation.
% Generally, the way you describe the fixed point iteration seems overly complicated -- why not just say you take existing estimates of the aggregates, calculate from these the probabilities with which fast and slow traders from each class go to market one, and then update the aggregates (possibly with some damping) by working out the total number of traders going to each market?@@


\subsection{Deterministic dynamics for the slow traders}
In this chapter, to determine the behaviour of traders, we rely on the homogeneous population dynamics of slow traders.\myMarginnote{role of the timescale difference}
Thanks to the fact that the relaxation time of slow traders is much longer than the relaxation time of fast traders, the fluctuations in their preferences are very small. In the limit $r\to 0$ they can be neglected and we can write the following equation for the deterministic dynamics of the preferences for slow traders.
\begin{equation}\myMarginnote{deterministic dynamics}
  \label{eq:detdyn}
  \dot{\Delta A}^{(c)} = \mu^{(c)}\left(\Delta A^{(c)},f_1(\Delta A^{(1)},\Delta A^{(2)}), f_2(\Delta A^{(1)},\Delta A^{(2)})\right)
\end{equation}
where the aggregates $f_m(\Delta A^{(1)},\Delta A^{(2)})$, $m \in \{1,2\}$ are the ratios of buyers over sellers at market $m$. To deduce the fraction of slow traders in both markets we use the score differences, and to deduce the fraction of fast traders in both markets we use the fixed point equation~\eqref{eq:het:fast_traders}.
Thanks to the timescale difference between the fast traders and the slow traders, we do not need to take into account the time needed by the fast traders to relax to the fixed point of Eq.~\eqref{eq:het:fast_traders} which is confirmed by the consistency between the simulation results and our analytics shown in Fig.~\ref{fig:het:fpvssimu}.
\begin{figure}[t!]
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/FpVsSimulation.pdf}
  \caption[Comparison between the Fokker Planck distribution obtained with the fixed point method described in Sec.~\ref{sec:het:distslow} and multiagent simulations]{Comparison between the Fokker Planck distribution obtained with the fixed point method described in Sec.~\ref{sec:het:distslow} and multiagent simulations. The preferences to buy of the traders in the two classes are $\pb^{(1)} = 1 - \pb^{(2)} = 0.2$, the market biases are $\theta_1 = 1 - \theta_2 = 0.3$, the forgetting rate is $r = 0.05$ and half of the traders are fast. The number of traders (including both fast and slow traders) is $10^4$ in the multiagent simulation.}
  \label{fig:het:fpvssimu}
\end{figure}
%\subsection{Fixed point equation for the slow traders}

\subsection{Onset of strong segregation}
In the above setup of a system of fast and slow traders, the notion of segregation -- as a separation of a population into subgroups that have different preferences over long timescales -- clearly makes sense only for the slow traders.
To assess the existence of weak segregation among these traders, as in Chap.~\ref{chap:Dynamic} and~\ref{chap:3M}, one just needs to count the number of peaks in the distribution of scores \ie{} the number of fixed points of the single traders' dynamics.
For strong segregation however, we need to know for each of the fixed points whether it corresponds to a peak of order $1$ or to an exponentially small peak.
To assess whether the system is strongly segregated or otherwise, we need to calculate the action difference between the two (or more) fixed points of the dynamics as explained in Fig.~\ref{fig:Dyn:selfcon}.
Luckily for a one-dimensional Langevin dynamics such as the one modelled in Eq.~\eqref{eq:het:kmexp_slow} the minimal action path can be calculated analytically.
This means that considering the stable fixed points of the single traders' dynamics $\Delta A_1^\star $ and $\Delta A^\star_2$, the conditions that the action difference between those two paths is zero (see Sec.~\ref{sec:Dyn:method}) is equivalent to:
%@@ check that the notation for action differences is consistent - don't you use $\Delta \mathcal{S}$ elsewhere? @@
\begin{equation}
  \label{eq:het:actiondiff}
  \Delta {\cal A}_{1 \to 2} = \int_{\Delta A^\star_1}^{\Delta A^\star_2} \frac{\mu(y,f_1, f_2)}{\Sigma(y,f_1, f_2)}{\rm d}y = 0 
\end{equation}
where we have dropped the $(c)$ superscript indicating the class to lighten the notation.
Using the equation above, we find the strong segregation temperature by plotting the action difference between the fixed points of the single trader dynamics when the system is weakly segregated. When the quantity $\Delta {\cal A}_{1 \to 2}$ is equal to zero, this means we are at the strong segregation threshold.
\section{Results}
\label{het:sec:results}
\begin{figure}[t!] 
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/VarieFractionOfSlow.pdf}
  %% The code to plot this image is in the notebook ~/Documents/analytics/HeterogeneousAgents/PlotsHeterogeneousAgents.ipynb
  \caption[Segregation thresholds as a function of the fraction of slow traders]{Segregation thresholds for the slow traders as a function of the fraction $f$ of slow traders when the fast traders have the same intensity of choice as the slow traders. %and when the slow traders have an intensity of choice $\beta_{\mathrm{fast}} = 0$ (red lines).
    Market biases: $\theta_1 = 1 - \theta_2 = 0.3$ and symmetric probabilities to buy $\pb^{(1)} = 1 - \pb^{(2)}$.
  }
  \label{fig:het:vafiefrac}
\end{figure}
\subsection{Heterogeneity in memory}
\label{sec:heterogeneity-memory}
To get an intuition for the influence of memory heterogeneity on the spontaneous emergence of segregated preferences, we looked at the variations of the segregation thresholds (both strong and weak) with the fraction of traders who only remember the last trading round ($r=1$).
In Fig.~\ref{fig:het:vafiefrac} we plot the segregation thresholds for the slow traders \ie{} $r \ll 1$.
We observe that \emph{the critical inverse intensity of choice $1/\beta_{\rm c}$ of both the strong and weak segregation thresholds decreases with the fraction of slow traders}.
This is certainly due to the slow traders taking advantage of the fast traders.
As a consequence, they earn more profit at their profit oriented market and will prefer to trade there (see Fig.~\ref{fig:het:distrib}).\myMarginnote{slow traders take advantage of the fast traders}
We also observe in Fig.~\ref{fig:het:vafiefrac} that when the fraction of slow traders is close to zero, the strong segregation threshold becomes small and has a strong dependence on $f$.
%, the fraction of slow traders.
Quantitatively, Fig.~\ref{fig:het:vafiefrac} shows that adding fast traders decreases the strong segregation threshold quite drastically, from $1/\beta_{\rm c} \approx 0.28$ for a population consisting exclusively of slow traders, to $1/\beta_{\rm c} \approx 0.15$ when the population contains only $20\%$ of slow traders.
This is not the case for weak segregation for which the segregation threshold only changes from $1/\beta_{\rm c}  \approx 0.28$ when the population has only slow traders to $1/\beta_{\rm c} \approx 0.259$ when the population is composed of $10\%$ of slow traders and $90\%$ of fast traders.


The above observations can be explained by the fact that, to strongly segregate, traders have to self-organise  in such a way that the buyers/sellers ratio in each market verifies self-consistency conditions similar to those in Chap.~\ref{chap:Dynamic}.
But when the fraction of slow traders is decreased, the range of aggregates that can be reached gets smaller
% (see Fig.~\ref{fig:het:marketcondZone}).
\myMarginnote{restriction of the attainable market conditions for slow traders}
because the influence of self-organisation of the slow traders on the buyers/sellers ratio in the markets decreases. We show this in Fig.~\ref{fig:het:marketcondZone} where the possible buyer/seller ratios in each of the markets are plotted for different fractions of slow traders.

\begin{figure}[t!]
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/AttainableMarketConditions.pdf}
  \caption[Buyers/sellers ratio which can be reached by the slow traders if they self-organise when the total population has a fraction $f$ of slow traders, and a fraction $1-f$ of fast traders]{Buyers/sellers ratio which can be reached by the slow traders if they self-organise when the total population has a fraction $f$ of slow traders, and a fraction $1-f$ of fast traders that have an intensity of choice $\beta = 0$, \ie{} choose market $1$ exactly half of the time.
  As expected, as the fraction of slow traders decreases, the zone of ``available'' market condition gets smaller and smaller. The traders' probabilities to buy and the market biases are the same as in Fig.~\ref{fig:het:vafiefrac}}
  \label{fig:het:marketcondZone}
\end{figure}




%@@ you need to say something about figure 4.5 here? seems not to be discussed anywhere @@
Another point of interest is how the preference distribution changes as the fraction of slow traders varies.
To answer this question, we plotted in Fig.~\ref{fig:het:distrib} the distribution of the preferences of the slow traders in the steady state of their learning dynamics.
We observe that as the fraction of slow traders decreases, \ie{} the fraction of fast traders increases, there are more and more traders with negative preferences (they prefer to trade at the profit oriented market).
This is a sign that the slow traders are taking advantage of the fast traders which enables them to trade more often at their profit oriented market. 



\begin{figure}
  \centering
  \includegraphics[width=1.05\textwidth, left]{02_Chapters/Hetergeneous_memory/Distributions.pdf}
  \caption[Distribution of the preferences of the slow traders for different fractions of fast traders in the population, as obtained from numerical simulations]{
    Distribution of the preferences of the slow traders for different fractions of fast traders in the population, as obtained from numerical simulations.
    We can see that as the fraction of fast traders increases, the number of slow traders with a preference for the profit oriented market increases.
    The other parameters are the same as in Fig.~\ref{fig:het:fpvssimu}.
  }
  \label{fig:het:distrib}
\end{figure}


\begin{figure}
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/VarieMarketBias.pdf}
  %% The code to plot this image is in the notebook ~/Documents/analytics/HeterogeneousAgents/PlotsHeterogeneousAgents.ipynb
  \caption{
    Thresholds for both strong (full line) and weak (dashed line) segregation as a function of $\pb^{(1)} = 1 - \pb^{(2)}$ (left panel) and $\theta_1 = 1 - \theta_2$ (right panel).  
  }
  \label{fig:het:varmarketbiases}
\end{figure}

We now look at how the thresholds shown in Fig.~\ref{fig:het:vafiefrac} vary depending on the market biases and the probability to buy of the traders $\pb^{(1)} = 1 - \pb^{(2)}$.\myMarginnote{varying the market bias}
In Fig.~\ref{fig:het:varmarketbiases}(a,b) we show the segregation thresholds for a population composed of only slow traders, a population formed of half slow and half fast traders and a population composed of $20\%$ of slow traders and $80\%$ of fast traders.
For this range of parameters, we observe that the segregation thresholds decrease with the fraction of slow traders.
We also observe in Fig.~\ref{fig:het:varmarketbiases}(a,b) 
that the effect of the variation of both the market biases and the probabilities to buy is larger on the strong segregation threshold than on the weak segregation threshold.
This suggests that the effects described in the previous paragraph for  $\theta_1 = 1 - \theta_2 = 0.3$ and $\pb^{(1)} = 1 - \pb^{(2)}$ should generalise to other values of $\theta_1 = 1 - \theta_2$ and $\pb^{(1)} = 1 - \pb^{(2)}$.
Moreover, extrapolating in Fig.~\ref{fig:het:varmarketbiases}(b) to $\theta_1 = 0.5$ suggests -- and this is confirmed by our numerics -- that for two fair markets % @@ it is a bit odd that you're not showing the range up to $\theta_1=0.5$ in the figure, given this discussion; if you don't have time to change the figure, I'd say "Moreover, extrapolating Fig... to $\theta_1=0.5$ suggests -- and this is confirmed by our numerics -- that in this limit, the fraction..." @@
when $\theta_1 = \theta_2 = 0.5$, the fraction of fast traders does not affect the segregation thresholds. The reason behind this is the symmetry of the markets.
Indeed, in such a case, slow and fast traders both go to the first and the second market with probability one half each.
This keeps the ratio of buyers over sellers to $1$ in each market, independently of the intensity of choice of the fast and slow traders and the fraction of fast traders. As one then moves away from the case of two fair markets, by decreasing $\theta_1 = 1 - \theta_2$, the effect of the fast traders on the segregation of the slow traders is \emph{continuously increasing}.
\subsection{Heterogeneity in the intensities of choice and memory length}
\label{sec:heter-intens-choice}
\begin{figure}
  \centering 
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/VarieTemperatureOfFastAndSlow.pdf}
  %% The code to plot this image is in the notebook ~/Documents/analytics/HeterogeneousAgents/PlotsHeterogeneousAgents.ipynb
  \caption[Strong and weak segregation thresholds for the slow traders depending on the intensity of choice of the fast traders, $\beta_{\rm fast}$, for classes with two different fractions of slow traders as shown]{Strong (full line) and weak (dashed lines) segregation thresholds for the slow traders depending on the intensity of choice of the fast traders, $\beta_{\rm fast}$, for classes with two different fractions of slow traders as shown. The thin lines indicate for one case the strong segregation thresholds in the two limits where the fast traders best respond to their preferences ($\beta_{\rm fast} = \infty$) and where they choose randomly ($\beta_{\rm fast} = 0$). In the plots, the probabilities to buy of traders are $\pb^{(1)} = 1 - \pb^{(2)} = 0.2$ and the market biases are $\theta_1 = 1 - \theta_2 = 0.3$.
  The grey line corresponds to $\beta_{\rm fast} = \beta_{\rm slow}$ and its intersection with the strong and weak segregation curves gives the segregation thresholds shown in Fig.~\ref{fig:het:vafiefrac} for $\pb^{(1)} = 0.2$.}
  \label{fig:het:varieic}
\end{figure}
To complete this initial analysis we investigate, in Fig.~\ref{fig:het:varieic}, how the segregation threshold varies when not just the memory length but also the intensity of choice of the fast traders is different from that of the slow traders.\myMarginnote{heterogeneity in the intensity of choice}
The first comment to make is that the segregation threshold increases with the inverse intensity of choice of the fast traders $1/\beta_{\rm fast}$.
We also see that while changes in the intensity of choice of the fast traders have almost no influence on the weak segregation threshold, their influence on the strong segregation threshold is much more substantial.
This is in line with our previous observations that the presence of the fast traders has more influence on the strong than on the weak segregation.

\subsection{Heterogeneity among the classes of traders}
\label{sec:heter-among-popul}
% @@ I think we've generally used "class" for the two types of traders and would suggest to stick to this, rather than use "population". I've changed this in a few places but probably not everywhere - please check and change as necessary. @@
\begin{figure}[t!]
  \centering
\includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/BuyersAreTheOnlyFast.pdf}
  %% The code to plot this image is in the notebook ~/Documents/analytics/HeterogeneousAgents/PlotsHeterogeneousAgents.ipynb
  %% \todo: use command tight_layout
  \caption[The strong and weak segregation thresholds when \emph{all the traders in the first class are fast} and all the traders in the second class are slow]{The strong (full line) and weak (dashed line) segregation thresholds when \emph{all the traders in the first class are fast} and all the traders in the second class are slow. We consider both the case of fast and slow traders having the same intensity of choice $\beta_{\rm fast} = \beta_{\rm slow}$ (blue curve) and the case where the fast traders have a zero intensity of choice $\beta_{\rm fast} = 0$ \ie{} they choose their next market randomly. The segregation thresholds for a system with the same number of fast traders with $\beta_{\rm fast} = \beta_{\rm slow}$ evenly distributed between classes, and for a system with only slow traders, are shown for comparison.}
  \label{fig:het:Pop1Fast}
\end{figure}
\begin{figure}[t!]
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/Hetergeneous_memory/VaryBothFracsOfFast.pdf}
  %% The code to plot this image is in the notebook ~/Documents/analytics/HeterogeneousAgents/PlotsHeterogeneousAgents.ipynb
  %% \todo: use command tight_layout
  \caption[Inverse intensity of choice at which weak and strong segregation is observed for the first time, as a function of the fraction of slow traders in both of each class $c$, $f_{\rm slow}^{(c)}$.]{Inverse intensity of choice at which weak (left panel) and strong (right panel) segregation is observed for the first time, as a function of the fraction of slow traders in both of each class $c$, $f_{\rm slow}^{(c)}$.
    The parameters used for this plot are $\theta_1 = 1 - \theta_2 = 0.3$ and $\pb^{(1)} = 1 - \pb^{(2)} = 0.2$.
    The white squares on the right indicate parameter values  where we could not calculate the strong segregation temperature.}
  \label{fig:het:VarieFracPopsFast}
\end{figure}
In the previous subsection, we observed that adding the same number of fast traders in both classes decreases the segregation thresholds of the slow traders.
In this subsection, we want to check if this phenomenon persists when the fractions of slow traders in class $1$ and class $2$ are different.
We first look at the extreme case when all the traders in the first class are fast and all the traders in the second class are slow.
To do so, in Fig.~\ref{fig:het:Pop1Fast} we plot both the strong and weak segregation thresholds for this scenario.
In order to understand the influence of the distribution of the slow traders among the classes, we also plot the segregation thresholds when there is the same overall fraction of fast traders in the population ($50\%$) and they are evenly distributed in the two classes. 
The first thing we note is that when class one is composed of only fast traders and class two is composed of only slow traders, both the strong and weak segregation thresholds are lower than when there are $50\%$ of slow traders in each class.
This suggests that an imbalance in the fraction of slow traders between the two classes impedes segregation. 
In fact, for the parameters we use in Fig.~\ref{fig:het:Pop1Fast}, once the puying probability $\pb^{(1)}$ of class 1 is larger than $\approx 0.25$, strong segregation no longer takes place at any intensity of choice of the slow traders.
As is shown in Fig.~\ref{fig:het:Pop1Fast} this is a phenomenon which we observe only for much higher values of $\pb^{(1)}$ when both classes contain $50 \%$ of fast traders.
To summarise the results of this discussion, not just 
the total fraction of fast traders but also the distribution of fast traders among the two classes has to be taken into account to understand segregation behaviour.

The findings of the previous paragraph make one wonder about the variations of the segregation thresholds with the fraction of traders in both the first and second classes. We show some results for this in Fig.~\ref{fig:het:VarieFracPopsFast}.
Since the market biases as well as the probability of buying of the traders are symmetric, the density plots of Fig.~\ref{fig:het:VarieFracPopsFast} are symmetric under interchange of $f_{\rm slow}^{(1)}$ and $f_{\rm slow}^{(2)}$.
From those plots, we can identify two factors that influence segregation: (i) the difference between the fraction of fast traders in class 1 and in class 2, which tend to decrease the segregation thresholds and (ii) the overall fraction of fast traders in the whole population, which tends to decrease the segregation thresholds as well. 

\section{Conclusion}
In this chapter, we investigated the persistence of segregation in a population where the intensity of choice, as well as the memory of the traders, are heterogeneous.\myMarginnote{summary}
After having introduced  in Sec.~\ref{sec:HetMem:introduction} our motivation, which is to take into account information heterogeneity in our model, we discussed how we model such heterogeneity in Sec.~\ref{het:sec:theory}.
Then, we explored the consequences of various sources of heterogeneity in Sec.~\ref{het:sec:results}, proceeding in three stages.
In Sec.~\ref{sec:heterogeneity-memory} we observed that adding the same fraction of traders with short memory in each class decreases the segregation thresholds for the traders with a long memory (see Fig.~\ref{fig:het:varieic}).
We saw in particular that the variations with respect to the fraction of fast traders added are much larger for the strong than for the weak segregation threshold.  
In Sec.~\ref{sec:heter-intens-choice} we investigated the effects of adding heterogeneity in the intensity of choice and observed that a larger intensity of choice for fast traders decreases the segregation thresholds of slow traders.
In Sec.~\ref{sec:heter-among-popul} we explored how the segregation thresholds vary when the fraction of traders with short memory is different within the two classes. While showing some non-trivial structure, the results conform to the general trend that adding traders with short memory tends to lower the segregation thresholds (see Fig.~\ref{fig:het:vafiefrac}).
We argued that this is because the addition of fast traders reduces the range of possible market conditions that slow traders can reach by self-organising (see Fig.~\ref{fig:het:marketcondZone}).
The fact that the effect described above is much stronger for strong rather than for weak segregation (it can even make strong segregation disappear as observed in Fig.~\ref{fig:het:varmarketbiases}) confirms this intuition.

A natural continuation of this work would be to add even more sources of heterogeneity in the population of traders.
For example, in the model we studied, we only considered the effect of traders with two different memory lengths while one could think of adding even more different values of the memory length.
Then, it would be interesting to check if the non-monotonicity of the payoffs with respect to the information of traders observed by Toth~\etal{} in~\cite{tothscalas} also takes place for a population composed of traders with several different memory lengths.
It would also be appealing to see if the phenomenon of segregation persists when traders learn their favorite market with a different algorithm.
Some suggestions for alternative learning dynamics can be found in~\cite{cesa2006prediction,fudenberg1998theory}. 

Another interesting extension of this work would be to add more heterogeneity in the buying probabilities of the traders.
In the models presented in Chap.~\ref{chap:Dynamic} and~\ref{chap:3M} as well as this chapter we only considered a population divided into two classes of equal size, each of which has a specific probability to buy $\pb$.
It would be interesting to see what happens when the probability to buy for each trader is fixed randomly from some nontrivial distribution at the beginning of each simulation \ie{} quenched, and how this influences both the strong and weak segregation thresholds.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../MainFile"
%%% End:
