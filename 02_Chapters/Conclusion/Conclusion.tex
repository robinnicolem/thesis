\chapter{Conclusion}
\label{chap:Conclusion}
\section{Summary of the results}
In this thesis we used large deviation methods to understand segregation and fixation in game theoretical models.
Chaps.~\ref{chap:Dynamic}, \ref{chap:HetMem} and~\ref{chap:3M} were concerned with the application of Freidlin-Wentzell theory to quantify the emergence of segregation for traders with an infinitely long memory.
Chap.~\ref{chap:popideas} dealt with the application of Kramers' rate theory, which is a simplified version of Freidlin-Wentzell theory for conservative dynamics, to study finite size quantities in birth-death processes.

The aim of Chap.~\ref{chap:Dynamic} was twofold: (i) comparing the steady state of some EWA learning dynamics to a simple game theoretical model and (ii) studying how the emergence of segregation is influenced by the use of fictitious play.
To fulfill aim (i) we built a simple aggregative game in Sec.~\ref{sec:Dyn:mf}, which turns out to have an infinity of Nash equilibria.
We classified each of those Nash equilibria according to the type of strategies played by the traders (pure or mixed).
In the rest of the chapter, we then linked the Nash equilibria to the actual long run outcome of EWA learning dynamics.
We first investigated the outcome of EWA learning dynamics when the fictitious play coefficient $\alpha$ is zero, traders have infinite memory $r \to 0$ and they best-respond to their preferences ($\beta \to \infty$).
We found in this case that the steady state of EWA learning is a homogeneous mixed Nash equilibrium \ie{} unimodal distribution of scores where all the traders play a mixed strategy.
We then extended our analysis and studied the long run outcome of the EWA learning dynamics when the limits $\alpha \to 0$ and $\beta \to \infty$ are taken simultaneously. As shown in Fig.~\ref{fig:Dyn:alphac}, depending on how the limit is taken, the corresponding long run outcome of the EWA learning dynamics will have qualitatively different properties (see Fig.~\ref{fig:Dyn:diff_ne}). 
Those include homogeneous-mixed states where all traders within a classe randomize in the same way as well as heterogeneous-pure states where traders split into two groups each choosing a market deterministically. Along with these standard types of Nash equilibria, we also found a heterogeneous mixed steady states where traders splits into groups but not all groups play deterministically.

In Chap.~\ref{chap:3M} we used the equal action formalism developed in Chap.~\ref{chap:Dynamic} to study strong segregation of traders with fixed buy/sell preferences across more than two markets.
Motivated by the wide variety of qualitatively different scores distributions that we observed in multi-agent simulation (see Fig.~\ref{fig:3M:SimulationIntro}), in Secs.~\ref{sec:3m:ThreeFaireMarkets} and~\ref{sec:3M:explo-param} we explored segregation across three markets for different ranges of parameters.  
Sec.~\ref{sec:3m:ThreeFaireMarkets} was dedicated to the analysis of segregation across three fair markets, \ie{} with $\theta_1 = \theta_2 = \theta_3 = 0.5$; this is the only set of market biases for which we found segregation of both classes of traders into three groups.\myMarginnote{summary}
Sec.~\ref{sec:3M:explo-param} was concerned with the exploration of different ranges of market parameters to understand the causes that drive segregation.
Based on this study, we inferred two plausible triggers for the emergence of segregation across three markets: (i) \emph{the closeness between the markets}, (ii) \emph{the average volume of trade and the profit earned in this market}.


Chap.~\ref{chap:HetMem} was concerned with the existence of segregation in a population where the intensity of choice, as well as the memory, are heterogeneous.
In Sec.~\ref{het:sec:theory} we detailed our approach to model heterogeneity.
The largest section of the chapter, Sec.~\ref{het:sec:results}, was concerned with the consequences of various sources of heterogeneity.
In Sec.~\ref{sec:heterogeneity-memory} we observed that adding the same fraction of traders with short memory in each class decreases the segregation thresholds, \ie{} suppresses segregation, for the traders with a long memory.
Specifically, we saw that the variations with respect to the fraction of fast traders are significantly larger for the strong than for the weak segregation threshold.
Sec.~\ref{sec:heter-intens-choice} investigated the effects of adding heterogeneity in the intensity of choice and we observed that decreasing the inverse intensity of choice of fast traders decreases the segregation threshold of slow traders.
Finally, Sec.~\ref{sec:heter-among-popul} explored how the segregation thresholds vary when the fraction of traders with short memory is different within the two classes.
Although the results showed some non-trivial structure, our results were in line with the general trend that adding traders with short memory lowers the segregation threshold (see Fig.~\ref{fig:het:vafiefrac}).
We believe that this behaviour arises because adding fast traders reduces the range of possible market conditions that slow traders can reach by self-organising (see Fig.~\ref{fig:het:marketcondZone}). 

In Chap.~\ref{chap:popideas} we interpreted learning as a pairwise comparison process within populations of ideas.
While for large populations, the dynamics is described by the \SC equation, there has been (to our knowledge) no systematic derivation of the \SC equation as the large size limit of a birth-death process.
Our work aimed at filling this gap by defining a birth-death process that leads to the \SC equation in the limit of a population of large size.
To do so we constructed in Sec.~\ref{subsec:symmetric_construction} and~\ref{subsec:asymmetric_construction} a birth-death process where the fitness of individuals was augmented by a term proportional to the information content of species $i$ $(- \ln x_i )$.
One of the outcomes of this work was to show that although the replicator-mutator dynamics and the \SC dynamics have fairly similar behaviour, their stochastic extensions have qualitatively different properties.
We also investigated the finite size quantities of the dynamics of the population of ideas in a number of symmetric and asymmetric games as a function of the memory-loss parameter $\lambda$.





\section{Future work}
\subsection{Technical improvements}
\paragraph{The robustness of the action minimization algorithm:}
The algorithm we used to study strong segregation phenomena
in Chaps.~\ref{chap:Dynamic} and~\ref{chap:3M} is suitable for our parameter regime.
However, for small values of the fictitious play coefficient, such as the one we were interested in in Chap.~\ref{chap:Dynamic}, the algorithm can fail to find the minimal action path (\ie{} it
does not converge).
This prevented us from investigating very small values of the fictitious play coefficient $\alpha$.
Therefore, a natural direction for future work would be to improve the robustness of the action minimisation algorithm for small $\alpha$. 
To do so we see two possible directions, either (i) improving the
algorithm used to minimise the discretised minimal action path or (ii) directly solving the
Hamilton-Jacobi equation that has as its solution the minimal action path.
The latter turns out to be challenging because it requires
the solution of a boundary value problem that is generally tackled with the "shooting method"~\cite{Press:2007:NRE:1403886}. 
However, in preliminary numerical experiments we found that the shooting method was not robust enough, which lead us to the use of the discretized path instead. 
An alternative technique to perform the shooting method could be based on linearising in small changes to the initial condition for the optimal path, from which iterative corrections to these initial conditions can then be estimated.
However, this technique is still being developed and the method was not yet robust enough to be used in the numerics for this thesis. 

\paragraph{Assessing the segregation via the distribution of scores:}
The advantage of the action minimisation methods we deployed is that they
enable one to assess the existence, or otherwise, of segregation in the large memory limit.
However---as explained above---in certain parameter regimes, the action minimisation algorithm may not converge.
In such situations, one can assess the existence of segregation by looking at the distribution of scores in multi-agent simulations.
One possible measure employed by Alori\'c~\cite{aloric2015emergence,thesisaleksandra} is the Binder cumulant~\cite{Binder1981}. 
This coefficient gives helpful information about the bimodality of the score distribution when the peaks are of equal size but are of less useful when the score distribution is composed of peaks with different size.
Alternatively, the existence of segregation can be tested using the DIP test for unimodality~\cite{hartigan1985}.
DIP compares the distribution of scores with the best-fitted uni-modal curve and returns a number between zero and one describing the similarity of the two curves.  
However, we see two challenges in using the DIP test for unimodality: (i) during our early investigation this test did not manage to separate strong and weak segregation (ii) as opposed to the Binder cumulant, we do not have any intuition on how the results of the DIP test generalise in the large memory limit $r \to 0$. Thus, it would be interesting to develop a generalised version of the DIP test that is adapted specifically to distributions with narrow peaks.

\paragraph{Asymptotics for the fixation time in populations of ideas}
In Chap.~\ref{chap:popideas} we calculate the fixation times for a large population of ideas undergoing a birth-death process.
We observed that the fixation time of such populations grows exponentially with its size.
It would be interesting to calculate this exponential growth rate with the memory size for both symmetric and asymmetric games. We have considered such an extension of our work and note the following:
\begin{itemize}
\item For symmetric games, one should use Kramers' rate theory.
  A challenge arises when
  applying this theory, however. One would need
  to go beyond the \KM{} expansion. If not, the resulting calculations would lead to an overestimate
  of the exponential growth rate of the fixation
  time~\cite{hanggi1984bistable,cianci2015wkb}.
\item Since the dynamics of
  asymmetric games is not conservative, here one would need to use a version of
  Freidlin-Wentzell theory that generalizes Kramers' rate theory
  to non-conservative systems~\cite{fwtheory,bouchetetal}.
\end{itemize}

\subsection{Extensions of the model}
In Chaps.~\ref{chap:Dynamic}, \ref{chap:3M} and~\ref{chap:HetMem}, we examined the existence or otherwise of segregation in different variations of a minimal model of double auction markets.
A logical continuation of this work is to explore further variations of the model among which we would suggest the following.

\paragraph{Adding heterogeneity in the traders' preferences}
Building upon the work of Chap.~\ref{chap:HetMem}, we could add additional heterogeneity in the behaviour of the traders.
For example, we could consider more than two different memory lengths within the population of traders.
In such a case, we expect the analysis of both the weak and strong segregation thresholds to be tractable as long as the orders of magnitude of the different timescales of the dynamics of the traders are different.

As a consequence of the large number of memory timescales, one might expect the payoff of traders to be non-monotonic with their memory as reported by Toth~\etal{} in~\cite{tothscalas}.
This monotonicity is related to the fact that highly informed traders can take advantage of the less informed ones but cannot take advantage of the uninformed traders who trade almost randomly.

Another exciting direction would be to have a population of traders that have different exploration/exploitation tradeoffs.
A straightforward way to implement such a behaviour would be to have traders with various intensities of choice $\beta$, building upon the model developed in Chap.~\ref{chap:HetMem}.


\paragraph{Varying the learning mechanisms}
The automated traders set up by the organizers of the CAT game that inspired our work learned their preferences using numerous different learning algorithms~\cite{cai2009overview}. 
Starting from this fact, we believe it would be interesting to vary the learning mechanisms of the traders.
This learning dynamics can be subdivided into how the traders make two separate choices: (i) the selection of their preferred market and (ii) the value of the order an agent sends to the market.

In our current model, we chose to have \emph{zero intelligence traders}, who do not learn the price of the orders they send to the market. 
This choice was motivated by the work of Gode and Sunders~\cite{gode1993allocative} who regard the ``Market as a Partial Substitute for Individual Rationality''.
An alternative proposed by Cliff \etal{} is the Zero-Intelligence-Plus~\cite{Cliff1997} learning algorithm (instead of zero intelligence~\cite{gode1993allocative}) as a potentially more accurate model traders' behaviour.
Other possible rules for traders to set their order price can be found in~\cite{RothErev} and~\cite{GJERSTAD19981}.

One could also study the robustness of the model against variation of the market selection strategy (reinforcement learning) our traders use.
A simplified approach would be to consider market selection as a multi-armed-bandit problem (MAB).
Among the existing learning algorithms used to solve such problems one can cite for example $\epsilon$-greedy strategy, and regret minimization.
This list is not exhaustive and there exist many other market selection strategies.
We refer the interested reader to a comprehensive review of MAB problems for more information~\cite{RLIntro,cesa2006prediction}.


Another exciting direction for our study would be to implement trader-trader interactions in our model.
In the vein of~\cite{DallAsta2012}, we could, for example, consider random networks of traders interacting with each other and seeing how the connectivity of the network influences the emergence of segregation.


\paragraph{Varying markets mechanisms}
In Chaps.~\ref{chap:Dynamic}, \ref{chap:3M} and~\ref{chap:HetMem}, we considered a simple model of discrete double auction markets. 
This made it possible to obtain analytical solutions for the segregation threshold in the large memory limit.
However, it would be interesting to generalize our results to a wider range of markets.
We could, for example, add transaction fees as in~\cite{soton273079} and make the markets adaptive as in~\cite{Robinson2012} or~\cite{cai2009overview}.
There are many other market mechanisms one could explore and we suggest~\cite{Niu2012,Parsons2006EverythingYW} as  an overview of the possible alternative market mechanisms that one could implement.


In real double auction markets, the traders pay for the service provided by the market.
This payment can come from the bid-ask spread, from additional fees charged for each transaction, etc. 
Those features are not yet implemented in our model, and they are good candidates for further extensions of our work as they could contribute to making the model more realistic.
One could, for example, imagine that the market charges a fixed fee for each transaction, or a fee proportional to the profit traders make, etc.

%Another promising application of our model might lie in the modelling of dark pools.
%One of the characteristics of dark pools is their opacity, which reduces the market impact of large orders compared to traditional double auction markets. 
%A model for dark pools would need to account for the fact that the supply/demand ratio, as well as the trading price in the markets we model, are not publicly available information.

\paragraph{Segregation in other aggregative games}
In Chap.~\ref{chap:Dynamic}, we observed segregation in a double auction market modelled as an aggregative game.
We believe it would be exciting to see whether such results can be generalized to other types of aggregative games such as the minority game~\cite{minoritygamebookCoolen,minoritygamebook} or the Cournot model for an oligopoly~\cite{cournotappl}.
One advantage of those games is that their simplicity should make it possible to obtain analytical results for the emergence of segregation using methods similar to those used in Chaps.~\ref{chap:Dynamic}, \ref{chap:3M} and~\ref{chap:HetMem}.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../MainFile"
%%% End:
