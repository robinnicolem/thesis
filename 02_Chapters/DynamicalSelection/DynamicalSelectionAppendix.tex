%\chapter{Appendix: Dynamical selection of Nash equilibria
% using Experience Weighted Attraction Learning}
\begin{subappendices}
\section{Formula for the payoff}
\label{app:Dyn:PayoffFormula}
To work out the average payoff of an ask (${\rm a}$) or bid (${\rm b}$) at market
$m$, we find first the probability for such an order to be
valid:
\begin{align}
  \mathcal{V}({\rm a},m) &= \mathbb{P}(\text{ask price $< \pi_m$}) = \frac{1}{\sqrt{2 \pi} \sigma} \int^{\pi_m}_{-\infty} \exp\left(- \frac{(x -\mua)^2}{2 \sigma^2}\right) {\rm d}x \\
  \mathcal{V} ({\rm b},m)&= \mathbb{P}(\text{bid price $> \pi_m$})= \frac{1}{\sqrt{2 \pi} \sigma} \int_{\pi_m}^{\infty} \exp\left(- \frac{(x - \mub)^2}{2 \sigma^2}\right) {\rm d}x
\end{align}
where the trading price $\pi_m$ is defined in equation \eqref{eq:Dyn:tp}.

Once an order has been validated, it needs to be matched with that of a trader on the other side of the market. We denote the probability for this to happen for an order of type $\tau$ at market $m$ by $\mathcal{M}(\tau,m,f_m)$. This quantity depends on the ratio of the number of buyers and sellers in the market, 
$f_m = \frac{\text{\# buyers @ market m}}{\text{\# sellers @ market m}}$, as follows:
\begin{align}
  \mathcal{M}(\mathrm{a},m,f_m) &= \min \left( \frac{f_m \mathcal{V} ({\rm b},m)}{\mathcal{V} ({\rm a},m)},1\right) \label{eq:Dyn:apm1}\\
  \mathcal{M}(\mathrm{b},m,f_m) &= \min \left( \frac{\mathcal{V} ({\rm a},m)}{f_m\mathcal{V} ({\rm b},m)},1\right)\label{eq:Dyn:apm2}
\end{align}
where the first ratio in the minimum is that of the number of \emph{valid} buy and sell orders, always assuming large $N$ where fluctuations of these numbers can be neglected.

We call $\langle \mathcal{S}_{\tau,m}\rangle$
the average score of an order of type $\tau$, once it has been validated and successfully matched.
This is given by:
\begin{align}
  \langle \mathcal{S}_{{\rm a},m}\rangle &= \frac{1}{\mathcal{V}({\rm a},m)}\frac{1}{\sqrt{2 \pi} \sigma}
                                           \int^{\pi_m}_{-\infty} \left(\pi_m -x \right) \exp\left(- \frac{(x - \mua)^2}{2 \sigma^2}\right)  {\rm d}x \\
  \langle \mathcal{S}_{{\rm b},m}\rangle &= \frac{1}{\mathcal{V}({\rm b},m)}\frac{1}{\sqrt{2 \pi} \sigma}
                                           \int_{\pi_m}^{\infty}  \left(x - \pi_m \right) \exp\left(- \frac{(x - \mub^2)}{2 \sigma^2}\right) {\rm d}x
\end{align}
For later use we also define the average square of the score:
\begin{align}
  \langle \mathcal{S}^2_{{\rm a},m}\rangle &= \frac{1}{\mathcal{V}({\rm a},m)}\frac{1}{\sqrt{2 \pi} \sigma}
                                           \int^{\pi_m}_{-\infty} \left(\pi_m -x \right)^2 \exp\left(- \frac{(x - \mua)^2}{2 \sigma^2}\right)  {\rm d}x \\
  \langle \mathcal{S}^2_{{\rm b},m}\rangle &= \frac{1}{\mathcal{V}({\rm b},m)}\frac{1}{\sqrt{2 \pi} \sigma}
                                           \int_{\pi_m}^{\infty}  \left(x - \pi_m \right)^2 \exp\left(- \frac{(x - \mub)^2}{2 \sigma^2}\right) {\rm d}x
\end{align}
We can now compute the average payoff of an order of type $\tau$ at market $m$:
\begin{align}
  \mathcal{P}_{\tau,m}(f_m) = \mathcal{V}(\tau,m) \mathcal{M}(\tau,m,f_m)  \langle \mathcal{S}_{\tau,m}\rangle
\end{align}
Similarly, the average squared payoff that will appear in the second order moment of the Kramers-Moyal expansion in App.~\ref{app:Dyn:kmexp} can be expressed as
\begin{align}
  \mathcal{Q}_{\tau,m}(f_m) &= \mathcal{V}(\tau,m) \mathcal{M}(\tau,m,f_m)  \langle \mathcal{S}^2_{\tau,m}\rangle\\
  \mathcal{Q}^{(c)}_{m}(f_m) &= \pb^{(c)}  \mathcal{Q}_{{\rm b},m}(f_m) + (1- \pb^{(c)}) \mathcal{Q}_{{\rm a},m}(f_m)
                               \label{eq:scoresquare}
\end{align}
The second version here is averaged over the preference for buying and selling of an agent in class $c$.


\section{Phase diagram boundaries in
  Fig~\ref{fig:Dyn:phidiag}}
\label{app:Dyn:BoundNe}
In this section we indicate how to calculate phase boundaries in Fig.~\ref{fig:Dyn:phidiag}, which shows the phase diagram for the case where the market bias and the probability to buy are symmetric ($\theta_1 = 1 - \theta_2$, $\pb \doteq \pb^{(1)} = 1 - \pb^{(2)} $).

At this boundary, a (symmetric) potentially heterogeneous Nash equilibrium (green triangle in Fig.~\ref{fig:Dyn:eqpayoff}) turns smoothly into a homogeneous pure equilibrium (blue diamond and orange square in Fig.~\ref{fig:Dyn:eqpayoff}) where the two classes of players choose different markets. One can therefore calculate the boundary by establishing the zone in the phase diagram where this homogeneous Nash equilibrium exists. For definiteness we consider the equilibrium $(\bar{p}^{(1)},\bar{p}^{(2)})=(1,0)$; the calculation for $(0,1)$ is completely analogous.


To get rid of the $\min$ in Eq.~(\ref{eq:Dyn:apm1},~\ref{eq:Dyn:apm2}) we focus in addition on the case where market 1
is saturated with sellers:
\begin{equation}
  \frac{f_1\mathcal{V}({\rm b},1)}{\mathcal{V} ({\rm a},1)} < 1
  \label{eq:Dyn:appmsat}
\end{equation}
As a consequence the $\min$ term disappears from the market
conditions:
\begin{align}
  \mathcal{M}(\mathrm{b},1,f_m) &=\mathcal{M}(\mathrm{a},2,f_1) = 1 \\
  \mathcal{M}(\mathrm{a},1,f_m) &= \mathcal{M}(\mathrm{b},2,f_2) =  \frac{f_1 \mathcal{V} ({\rm b},1)}{\mathcal{V} ({\rm a},1)}
\label{eq:Dyn:Match_probs}
\end{align}
Here the equality between $\mathcal{M}(\mathrm{a},1,f_1)$ and
$\mathcal{M}(\mathrm{b},2,f_2)$ comes from the symmetry of the
parameters. Because $(\bar{p}^{(1)},\bar{p}^{(2)})=(1,0)$, all agents from class 1  go to market 1 and so the buyer-to-seller ratios $f_m$ from \eqref{eq:Dyn:2} are simple to express in terms of $\pb$:
\begin{align}
  f_1 = \frac{1}{f_2} = \frac{\pb}{1-\pb}
\end{align}

The payoffs at the two markets for traders from class 1 simplify accordingly:
\begin{align}
  \mathcal{P}^{(1)}_1(f_1
) &= \pb  \mathcal{V}(\mathrm{b},1) \langle S_{{\rm b,1}}\rangle + (1- \pb ) \mathcal{V}(\mathrm{a},1)\left[\frac{\pb}{1-\pb} \frac{\mathcal{V}(\mathrm{b},1)}{\mathcal{V}(\mathrm{a},1)}\right]  \langle S_{{\rm a,1}}\rangle \label{eq:Dyn:apppo1}\\
  \mathcal{P}^{(1)}_2(f_1
) &= (1-\pb)  \mathcal{V}(\mathrm{a},2) \langle S_{{\rm a,2}}\rangle +  
\pb \mathcal{V}(\mathrm{b},2) \left[\frac{\pb}{1-\pb} 
\frac{\mathcal{V}(\mathrm{b},1)}{\mathcal{V}(\mathrm{a},1)}\right]
\langle S_{{\rm b,2}}\rangle \label{eq:Dyn:apppo2}
\end{align}
The factors in brackets are the matching probabilities from \eqref{eq:Dyn:Match_probs}, from which $\mathcal{V}(\mathrm{a},1)$ cancels in the first equation and
similarly (by symmetry) $\mathcal{V}(\mathrm{a},1) = \mathcal{V}(\mathrm{b},2)$ in the second.

Our assumed equilibrium $(\bar{p}^{(1)},\bar{p}^{(2)}) = (1,0)$ will be a Nash equilibrium if the payoff at market 1 is higher than at market 2 for 
players from class 1. (By symmetry, the payoff relation is then reversed for players in class 2.)
From the explicit payoff expressions above, this condition can be re-arranged into
\begin{align}
  \label{eq:Dyn:secondordereq}
  0  &\le \pb^2 \left( -  \langle S_{{\rm a},2} \rangle  \mathcal{V}(\mathrm{a},2)-  \langle S_{{\rm b},1}\rangle  \mathcal{V}(\mathrm{b},1) -  \langle S_{{\rm a},1} \rangle \mathcal{V}(\mathrm{b},1) -  \langle S_{{\rm b},2} \rangle\mathcal{V}(\mathrm{a},2)\right) \notag\\
   &+ \pb \left(  \langle S_{{\rm b},1}\rangle  \mathcal{V}(\mathrm{b},1) + 2  \langle S_{{\rm a},2} \rangle  \mathcal{V}(\mathrm{a},2) +  \langle S_{{\rm a,1}}\rangle \mathcal{V}(\mathrm{b},1) \rangle\right) -   \langle S_{{\rm a},2} \rangle  \mathcal{V}(\mathrm{a},2)
\end{align}
For given $\theta_1$ all coefficients in this quadratic equation are known so the phase boundaries can be obtained directly as its roots. 
We plotted these roots in Fig.~\ref{fig:Dyn:appBoundNe}; note that the boundaries are close to linear but not exactly so.
One has to check a posteriori that the assumption \eqref{eq:Dyn:appmsat} of market 1 being saturated with sellers is valid, which rules out the bottom ``cone'' in the figure.

The remainder of the phase diagram in Fig.~\ref{fig:Dyn:phidiag} is obtained by the analogous calculation under the assumption that market 1 is saturated with buyers rather than sellers, which yields the bottom ``cone'' in Fig.~\ref{fig:Dyn:appBoundNe} and by finally repeating the overall reasoning for the Nash equilibrium $(\bar{p}^{(1)},\bar{p}^{(2)}) = (0,1)$.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/DynamicalSelection/BoundariesNashEq}
  \caption[Analytic determination of boundaries for the zone where a homogeneous Nash equilibrium exists where players from the two classes choose different markets]{Analytic determination of boundaries for the zone where a homogeneous Nash equilibrium exists where players from the two classes choose different markets. Within the blue regions the payoff inequality \eqref{eq:Dyn:eqpayoffcond} is satisfied. The region shaded grey is ruled out by the assumption of market 1 being saturated with sellers.
}
  \label{fig:Dyn:appBoundNe}
\end{figure}

\section{Kramers-Moyal expansion}
\label{app:Dyn:kmexp}
Here we provide the coefficients of the Kramers-Moyal expansion for
traders with fixed buy-sell preference, given fictitious play
coefficient $\alpha$ and intensity of choice $\beta$.  The truncation of the Kramers-Moyal expansion at the
second order gives the Fokker-Planck equation for the time evolution of the attraction distributions:
\begin{align}
  \label{eq:Dyn:app:km}
  \partial_t \mathbb{P}(\mathbf{A}^{(c)},t) &=
  - \sum_{1\le m \le 2  } \partial_{A^{(c)}_m}[\mu^{(c)}_m(\mathbf{A}^{(c)},\bar{p}^{(1)},\bar{p}^{(2)})\mathbb{P}(\mathbf{A}^{(c)},t)]\notag \\
  &+ \frac{r}{2} \sum_{1\le m,m' \le 2}\partial^2_{A^{(c)}_m A^{(c)}_{m'}} [\Sigma^{(c)}_{m m'}(\mathbf{A}^{(c)},\bar{p}^{(1)},\bar{p}^{(2)})\mathbb{P}(\mathbf{A}^{(c)},t)]
\end{align}
To lighten the notation we will in the following drop the superscript $(c)$ indicating the class of an agent
and also suppress the dependence on the aggregates $\bar{p}^{(1)},\bar{p}^{(2)}$, which are in general time-dependent via Eq.~\eqref{eq:Dyn:detdynaggr}.

In the above expansion time has been rescaled as $t=rn$, where $n$ is the number of trading rounds. The time interval $\Delta t = r$ then features in the normalization of the drift and diffusion matrix, which are determined as the first and second order jump moments:
\begin{equation}
\boldsymbol{\mu} = \frac{1}{r} \langle \Delta \mathbf{A}\rangle, \qquad
r\mathbf{\Sigma} = \frac{1}{r} \langle \Delta \mathbf{A}\,
\Delta \mathbf{A}^{\rm T}\rangle
\end{equation}
where $\Delta \mathbf{A}=\mathbf{A}(n+1)-\mathbf{A}(n)$ is the change in the agent's attraction vector in one training round and the T superscript indicates vector transpose. Writing $\Delta\mathbf{A}$ explicitly from \eqref{eq:Dyn:EWA_dynamics} then gives for the drift term:
\begin{align}
  \mu_1(\mathbf{A}) &= [\mathcal{P}_1(f_1) -  A_1] \sigma_\beta(A_1- A_2) -\alpha A_1 \sigma_\beta(A_2- A_1) \label{eq:Dyn:appdr1}\\
  \mu_2(\mathbf{A}) &= [\mathcal{P}_2(f_2) -  A_2] \sigma_\beta(A_2- A_1) -\alpha A_2 \sigma_\beta(A_1- A_2) \label{eq:Dyn:appdr2}
\end{align}
In the diffusion term $\Sigma_{ij}$ the second order
moments of the score distribution
also feature, as follows: 

\begin{align}
  \Sigma_{11} (\mathbf{A}) =& \left[\mathcal{Q}_1(f_1)  - 2A_1 \mathcal{P}_1(f_1) + {A_1}^2\right]  \sigma_\beta(A_1- A_2) \nonumber\\ &+
                                                                   \alpha^2 {A_1}^2  \sigma_\beta(A_2- A_1)\\
  \Sigma_{22} (\mathbf{A}) =& \left[\mathcal{Q}_2(f_2)  - 2A_2 {\mathcal{P}_2}(f_2) + {A_2}^2\right]  \sigma_\beta(A_2- A_1)\nonumber\\ & + 
  \alpha^2 {A_2}^2  \sigma_\beta(A_1- A_2) \\
  \Sigma_{12} (\mathbf{A})  =& -\alpha {\Big[}\mathcal{P}_1(f_1) A_2 \sigma_\beta(A_1- A_2) \nonumber\\&+
 \mathcal{P}_2(f_2) A_1\sigma_\beta(A_2- A_1) 
-A_1A_2{\Big]}\\
  \Sigma_{21} (\mathbf{A}) &=  \Sigma_{12} (\mathbf{A})
\end{align}





\section{Fixed points of single agent dynamics}
\label{app:Dyn:scalingbeta}
We show here generally that the single agent dynamics can have up to five fixed points, which can be determined from a single nonlinear equation. As before we drop the superscript $(c)$ for the agent class. The aggregates and hence the expected payoffs $\mathcal{P}_1$, $\mathcal{P}_2$ are fixed.

Fixed points are found from the condition that the drift (\ref{eq:Dyn:appdr1},\ref{eq:Dyn:appdr2})
must vanish: 
\begin{align}
  0 &= (\mathcal{P}_1 -  A_1) \sigma_\beta(A_1- A_2) -\alpha A_1 \sigma_\beta(A_2- A_1) \\
  0 &= (\mathcal{P}_2 -  A_2) \sigma_\beta(A_2- A_1) -\alpha A_2 \sigma_\beta(A_1- A_2)
\end{align}
Writing $\Delta=A_1-A_2$ and using $\sigma_\beta(A_2-A_1)=1-\sigma_\beta(\Delta)$, one can express $A_1$ and $A_2$ in terms of $\Delta$:
\begin{eqnarray}
A_1 &=& \frac{\mathcal{P}_1\sigma_\beta(\Delta)}{
\sigma_\beta(\Delta) + \alpha[1-\sigma_\beta(\Delta)]} \ =\  
\frac{\mathcal{P}_1}{
1+ \alpha \exp(-\beta\Delta)}
\\
A_2 &=& \frac{\mathcal{P}_2[1-\sigma_\beta(\Delta)]}{
1-\sigma_\beta(\Delta) + \alpha \sigma_\beta(\Delta)}
 \ =\  
\frac{\mathcal{P}_2}{
1+ \alpha \exp(\beta\Delta)}
\end{eqnarray}
Taking the difference gives a single equation for $\Delta$, which takes a suggestive form if we write $\alpha = \exp(-a\beta)$:
\begin{equation}
  \Delta = \frac{\mathcal{P}_1}{1 + \exp(-\beta (\Delta+a))} - \frac{\mathcal{P}_2}{1 + \exp(\beta (\Delta-a))}
  \label{eq:Dyn:appendix1}
\end{equation}
The solutions of this equation, and hence the single agent fixed points, can be obtained graphically by intersecting a straight line (the l.h.s.\ 
of Eq.~\eqref{eq:Dyn:appendix1}) with the function of $\Delta$ on the r.h.s. This function has a simple shape as it is the sum of two sigmoids, one increasing from zero to $\mathcal{P}_1$ around $\Delta=-a$ and the other increasing from $-\mathcal{P}_2$ to zero around $\Delta=a$. From the resulting shape, shown in Fig.~\ref{fig:Dyn:rhsapp}, at most five intersections with the diagonal can occur.

We are most interested in the limit of large 
intensity of choice $\beta$, where the sigmoids become step functions. For small $\alpha$, \ie\ large $a$, the only solution is then $\Delta=\mathcal{P}_1-\mathcal{P}_2$. As $\alpha$ is increased and hence $a$ is decreased, the sigmoidal steps move closer to the origin, each creating an additional pair of solutions when $a$ equals the relevant payoff (see Fig.~\ref{fig:Dyn:rhsapp}). For large $\beta$, one therefore has as transition from one to three (two stable, one unstable) fixed points at 
\begin{equation}
\alpha \sim \exp(-\max(\mathcal{P}_1,\mathcal{P}_2)\beta)
\end{equation}
and from three to five (three stable, two unstable) fixed points at
\begin{equation}
\alpha \sim \exp(-\min(\mathcal{P}_1,\mathcal{P}_2)\beta)
\end{equation}
At finite $\beta$ the fixed points are shifted away from $\Delta=\pm a$ and this would give corrections to $a$ of order $1/\beta$, which would in turn determine the prefactors of the above scalings.
Note that as $a$ decreases further, the two sigmoidal ramps will eventually overlap when $a$ is of order $1/\beta$, signalling a transition back to three (two stable) fixed points.
\begin{figure}[t!!]
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/DynamicalSelection/appendixDeltaEq}
  \caption{Sketch of the right handside of the fixed point equation \eqref{eq:Dyn:appendix1} for $\Delta$}
\label{fig:Dyn:rhsapp}
\end{figure}

We show in Fig.~\ref{fig:Dyn:alphac} that the scaling of the above $\alpha$-values, taken at equal payoffs $\mathcal{P}_1=\mathcal{P}_2$ as is relevant for Nash equilibria, also gives a good account of the variation with $\beta$ of $\alpha_c$ and $\alpha_c'$. 
This suggests that the $\alpha$-values where new fixed points appear, and where they contribute as peaks with weights of order unity to the steady state distribution, are relatively close, maybe only within a constant prefactor of each other.
\end{subappendices}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../MainFile"
%%% End:
