%%%%%\chapter{Appendix: Stochastic evolution in populations of ideas}
\begin{subappendices}
\section{Limits on birth-death description of Sato-Crutchfield learning}
\label{app:lambdac} 
The parameters $\Gamma$ and $\lambda$ of the stochastic evolution of ideas we have defined need to be chosen so that all transition rates $T_n^\pm$ in Eq. (\ref{eq:ratecomplete}) are non-negative. 
Except in the case of pure replicator dynamics ($\lambda=0$), this gives constraints on the parameters that depend on population size $N$, though weakly. The reason is the logarithmic term in the fitness~(\ref{modified_fitness}), which can get as large as $-\lambda\ln(1/N)$. 

For fixed $\Gamma$ the rates will only remain non-negative if $\lambda\leq \lambda_c$. One can compute a lower bound for $\lambda_c$. Firstly, all the transition rates will be positive if and only if the constraint
  \begin{equation}
    \label{eq:lambdac1}
    \left| \Delta \pi - \lambda \ln\frac{N-n}{n}\right| \le \frac{1}{\Gamma} 
  \end{equation}
  is met for all $0< n<N$. Since the quantity $\Delta \pi = \pi_1 - \pi_2 $ varies
  linearly with $n$, it is bounded by $\Delta \pi (1)$ and $\Delta \pi (N-1)$. Applying the triangular inequality to \eqref{eq:lambdac1}
  gives:
  \begin{equation}
    \label{eq;lambdac2}
    \left| \Delta \pi - \lambda \ln\frac{N-n}{n}\right|\le \max \left(\left|\Delta \pi (1)\right|, \left|\Delta \pi (N-1)\right|\right) + \lambda \ln (N-1)
  \end{equation}
  As a consequence, all transition rates are positive as long as $\max \left(\left|\Delta \pi (1)\right|, \left|\Delta \pi (N-1)\right|\right) + \lambda \ln (N-1) \le 1/\Gamma$. This translates into

  \begin{equation}
    \lambda \le \frac{1}{\ln (N-1)} \left(\frac{1}{\Gamma} - \max \left(\left|\Delta \pi (1)\right|, \left|\Delta \pi (N-1)\right|\right) \right)
  \end{equation}
The right-hand side therefore provides a lower bound on the critical value $\lambda_c$. This bound is plotted as a function of $N$ in Fig.~\ref{fig:lc}. While the bound goes to zero for $N\to\infty$, the inverse logarithmic dependence means it does so extremely slowly: the restriction on the allowed range of $\lambda$ is therefore mild even for very large population sizes ($N \sim 10^8$ and beyond).
\begin{figure}[t!]
  \centering
  \includegraphics[width=0.7\textwidth]{02_Chapters/PopulationOfIdeas/lambdac.pdf}
  \caption[Lower bound on $\lambda_c$ for a coexistence game as defined in section~\ref{sec:simple-examples-two}, for $\Gamma = 0.1$]{Lower bound on $\lambda_c$ for a coexistence game as defined in section~\ref{sec:simple-examples-two}, for $\Gamma = 0.1$. The bound (black line) is inversely proportional to the logarithm of the population size $N$.
  }
  \label{fig:lc}
\end{figure}

\section{Non-monotonicity of the fixation time in a coordination game}
\label{app:coordgame_2}
In Sec.~\ref{sec:simple-examples-two}, we studied fixation in a coordination game and observed that the fixation time is \emph{non monotonic} in $\lambda$ close to the bifurcation threshold $\lambda_c$, for \emph{small $N$}. We will provide an explanation for this phenomenon by decomposing the dynamics leading to fixation into a sequence of elementary events. When $N$ is small enough for activation times to be only moderate, beyond the bifurcation threshold, two additional effects come into play in addition to the relaxation and activation processes observed for large $N$: (i) direct activation: when starting near $x=0$, a fluctuation (activation event) can drive the system straight to fixation at $x=0$, even though the deterministic relaxation would take it in the other direction; (ii) trapping in regions near deterministic fixed points, where the net (deterministic) flow is low; deterministic relaxation times can then become comparable to activation times (precisely at such a fixed point, the deterministic relaxation time is in fact infinite as the flow vanishes). Finite populations will stay trapped in these regions of low deterministic flow for a long (but finite) time. This time will grow logarithmically with $N$ as explained in this appendix, Sec.~\ref{sec:fixat-regi-small}. Such regions exist at and near the bifurcation at $\lambda_c$, both for $\lambda$ below and above $\lambda_c$.

The curve in Fig.~\ref{fig:coord2}(b) for $\lambda=0.475$ shows the first effect: for small initial values of $x$, fixation times are rather low, as direct activation towards $x=0$ is the dominant fixation mechanism. To the right of the maximum in the curve, on the other hand, we have fixation predominantly at $x=1$. The fixation time here is, to a good approximation, given by the deterministic relaxation time to the stable fixed point close to $x=1$, with the final activation to $x=1$ being sufficiently fast to be sub-leading.

Accordingly, the sample trajectories in Fig.~\ref{figtrajcoord} show that the system moves to the stable fixed point in a close-to-deterministic fashion, with fixation at $x=1$ occurring shortly afterwards.

The second effect above contributes to the initial condition-dependence of the fixation time in Fig.~\ref{fig:coord2}. Here we are close enough to the bifurcation to have an extended region of low flow, causing a significant peak in the transition time curve. The low flow also makes fluctuation effects significant as explained above, and these cause deviations from the  times predicted for purely deterministic relaxation.
In Fig.~\ref{figtrajcoord}, the sample trajectories that start from $n=200$ ($x=0.2$)  illustrate this effect.

Finally, the low flow also makes direct activation to $x=0$ fast, giving a larger region of initial $x$ where this is the main fixation mechanism. As is clear from
Fig.~\ref{fig:coord2}(b), the resulting movement of the peak in the fixation time is what causes the non-monotonic $\lambda$-dependence at fixed initial condition that is visible in Fig.~\ref{fig:coord2}(a).
We refer to one of the two sample trajectories starting from $n=50$ ($x=0.05$) in Fig.~\ref{figtrajcoord} for an illustration of  a direct activation event.

We note that the direct activation effects discussed above for the coordination game do occur also for coexistence and dominance games, with the same consequence that fixation times become small for initial conditions near $x=0$. These other games do not have the additional features arising from the bifurcation in the coordination game, however, so do not show non-monotonic variation of the fixation time with $\lambda$.
\begin{figure}
  \centering
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/PopulationOfIdeas/coord_app.pdf}
  \caption[Different types of fixation dynamics in the  coordination
    game with the payoff matrix of Fig.~\ref{fig:payoffmat},
    superimposed onto the fixed point structure of
    Fig.~\ref{fig:coord}(a)]{Different types of fixation dynamics in the  coordination
    game with the payoff matrix of Fig.~\ref{fig:payoffmat},
    superimposed onto the fixed point structure of
    Fig.~\ref{fig:coord}(a). For values of $\lambda$ below the
    bifurcation threshold, the potential formalism allows one to
    identify three different zones [(i), (iv) and (iii), with the
    latter covering only a very narrow $\lambda$-range] with qualitatively different fixation dynamics; see Fig.~\ref{fig:cases}. Note that this subdivision into three zones cannot be deduced from the deterministic \SC dynamics and its fixed point structure (Sec.~\ref{sec:simple-examples-two}) alone.}
\label{fig:coord_app}
\end{figure}
\section{Activation dynamics in stochastic evolution of ideas for symmetric games}
\label{sec:furth-analys-stoch}
Here, we explain how to obtain the large $N$-behaviour of activation times in our stochastic evolution for a population of ideas, and discuss the consequences for the fixation dynamics.
\subsection{Kramers-Moyal expansion and effective potential}
\label{sec:kram-moyal-expans}
Our starting point is the dynamics defined by the transition rates \eqref{eq:basic_rates} and \eqref{eq:basic_rates2}. We have discussed in the main text how for $N\to\infty$ this leads to deterministic dynamics, here -- by our construction --  the Sato-Crutchfield equation (\ref{eq:scsym}). This can formally be derived from a Kramer-Moyal expansion to lowest order. 
In order to capture stochastic effects, one retains the first sub-leading order in the expansion. This is standard for
evolutionary processes \cite{traulsen2009stochastic}, and leads to an It\=o stochastic differential equation of the form 
\begin{equation}
  \label{eq:1}
  \dot{x}= h(x) + \frac{1}{\sqrt{N}}\sigma(x)\xi(t),
\end{equation}
where $\xi(t)$ is Gaussian white noise of unit variance,
$\avg{\xi(t)\xi(t')}=\delta(t-t')$. For the birth-death
process discussed in Sec.~\ref{sec:sym} one finds
\begin{subequations}
  \BE
  h(x) &=& \Gamma x (1-x) \left( \pi_1(x)-\pi_2(x)- \lambda \ln \left(\frac{x}{1-x}\right )\right), \\
  \sigma(x) &=& \sqrt{x(1-x)}.  \EE
\end{subequations}


Our aim is to use Eyring-Kramers theory \cite{hanggi1990reaction}, and
so we map the above dynamics with multiplicative noise to one with additive
noise. This is standard for systems with one degree of freedom, and is
achieved by a change of variable from $x$ to
\be y(x) \equiv \int_0^x
\frac{\mathrm{d}x'}{\sigma(x')} = 2 \arcsin(\sqrt{x}) \ee
and conversely
$x(y)=\sin^2(y/2)$. Translating the dynamics of $x$ to one for $y$ gives 
\begin{equation}
  \label{eq:2}
  \dot{y}(t) =\frac{h\left(x(y)\right)}{\sigma\left(x(y)\right)} - \underbrace{\frac{1}{2 N} \frac{\sigma'(x(y))}{\sigma^2(x(y))}}_{\text{neglected}}+ \frac{1}{\sqrt{N}} \xi(t)
\end{equation}
The additional flow term with prefactor $\frac{1}{N}$ arises from the $x$-dependence of the original noise variance $\sigma^2(x)$.
 We will see shortly that this term can be neglected in determining the leading (exponential in $N$) scaling of activation times.
The $y$-dynamics can now be written in the form
\begin{equation}
  \dot{y}(t) = -\Gamma\frac{dV_y}{dy} + \frac{1}{\sqrt{N}} \xi(t)\label{eq:3}
\end{equation}
with
\begin{align}
  V_y(y) &= -\frac{1}{\Gamma}\int_0^y
  \mathrm{d}y' \  \frac{h(x(y'))}{\sigma(x(y'))}   + \mathcal{O}(1/N)
\end{align}
Now that we have a standard Langevin equation with additive noise, Eyring-Kramers theory tells us that the time for an activated event, say from a stable fixed point $y_1$ to an unstable fixed point (barrier state) $y_2$ or to a boundary, scales as $\exp\{N\Gamma[V_y(y_2)-V_y(y_1)]\}$. It follows that the $\mathcal{O}(1/N)$ term in $V_y$ will only contribute to the prefactor, which we are not considering here anyway; it can therefore be neglected.
More importantly, if we translate back from $y$ to $x$ the potential takes the simple form
\begin{align}
V(x) = V_y(y(x)) &= 
-\frac{1}{\Gamma}\int_0^x
  \frac{\mathrm{d}x'}{\sigma(x')} \frac{h(x')}{\sigma(x')}\nonumber\\
& =
 - \int_{0}^{x}\mathrm{d}x'\left[ \pi_1(x')-\pi_2(x') - \lambda \ln \left( \frac{x'}{1-x'}\right)\right] 
\label{eq:apppot}
\end{align}
and activation times scale as
\be 
\tau \sim \exp\{N\Gamma[V(x_2)-V(x_1)]\}
\ee 
This will be the basis for our further analysis. In particular, we will exploit that for large $N$, differences in activation barriers $V(x_2)-V(x_1)$ translate into exponentially different timescales, hence if there are competing processes the one with the smaller activation barrier occurs first (with probability one as $N\to\infty$).

We add finally as a note of caution that the above Langevin analysis is valid for small $\Gamma$, where the rates for a transition $n\to n+1$ and its reverse are close to each other. Otherwise a more general approach is needed to determine activation timescales~\cite{hanggi1984bistable}.
 
\subsection{Generic symmetric two-strategy games}
\label{sec:analys-diff-symm}
We can write down the potential $V(x)$ quite generically for a symmetric game where there are two actions to choose from. 
Inserting the explicit form of the payoffs (see Eq.~\eqref{eq:payoffs1} and \eqref{eq:payoffs2}) into 
\eqref{eq:apppot}, one has, up to an umimportant additive constant,
\begin{equation}
  \label{eq:4}
V(x) =\tilde{v} \left(x-\frac{1}{2}\right)+\tilde{w} \left(x-\frac{1}{2}\right)^2 - \lambda s(x)
\end{equation}
Here the entropy is $s(x) = -x \ln(x) - (1-x) \ln(1-x)$ as before, and we have introduced the abbreviations
\begin{subequations}
  \begin{align}
    \tilde{  v} & = \frac{a_{21} + a_{22} - a_{12} - a_{11}}{2}\\
    \tilde{  w} & = \frac{a_{12} + a_{21} - a_{11} - a_{22}}{2}
  \end{align}
\end{subequations}
For $\lambda=0$ it is now easy to see the link to the three categories of symmetric game considered in Sec.~\ref{sec:simple-examples-two}, bearing in mind that all stationary points of $V(x)$ obey $h(x)=0$, hence are fixed points of the dynamics. For $w>0$ and $|v|<w$, $V(x)$ has a minimum in the relevant range $0\leq x \leq 1$, and we have a {\em coexistence} game. For $w<0$ and $|v|<|w|$, on the other hand, $V(x)$ has a maximum, corresponding to a {\em coordination} game. In the remaining cases, where $|v|>|w|$, $V(x)$ is monotonic for $x\in [0,1]$, so one has a {\em dominance} game.

To understand the effect of nonzero $\lambda$ on $V(x)$, note that the function $-\lambda s(x)$ is convex. 
Hence for a coexistence game $V(x)$ continues to have a single minimum $x^\star$. A fixation trajectory will first relax to this minimum. The barrier to activation towards $x=0$ is then $V(0)-V(x^\star)$, so fixation will occur there if this is lower than the corresponding barrier $V(1)-V(x^\star)$ for fixation at $x=1$. In the opposite case, i.e.\ for $V(0)>V(1)$, fixation will occur at $x=1$.

For a dominance game, the inclusion of the entropic term in $V(x)$ will create a single minimum $x^\star$ for any $\lambda>0$, because the derivative $-\lambda s'(x)$ diverges to $\pm \infty$ at the two boundaries $x=0$ and $x=1$. The fixation dynamics then follows the same pattern as for a coexistence game.
\subsection{Kramer-Moyal expansion for coordination games}
\label{app:coordgame}
The remaining case of coordination games is the most interesting, as the competition between the maximum in $V(x)$ at $\lambda=0$ and the convex entropic term can create additional minima. We keep $\lambda>0$ from now on and write 
\be
V(x) =\lambda\left[v \left(x-\frac{1}{2}\right)+w \left(x-\frac{1}{2}\right)^2 - s(x)\right]
\ee
with $v=\tilde v/\lambda$, $w=\tilde w/\lambda$. The shape of $V(x)$ is determined by these parameters, while $\lambda$ only affects the overall scale of the activation barriers but not their relative size for different processes. We therefore drop the prefactor $\lambda$ in the following.

 
\begin{figure}[t!!]
  \centering
  \includegraphics[width=0.85\textwidth, left]{02_Chapters/PopulationOfIdeas/pts.pdf}
  \caption{Graphical representation of the definitions of
    $\Delta_0 = V(0) - V(x_s)$, $\Delta_1 = V(1) - V(x_s)$,
    $B_0 = V(0) - V(x^\star_1)$ and $B_1 = V(1) - V(x^\star_2)$.}
  \label{fig:pot_ill}
\end{figure}
For large $v$ and $w$, corresponding to small $\lambda$ at fixed $\tilde v$ and $\tilde w$, the entropic term is mostly negligible in $V(x)$. But its diverging derivative always dominates in $V'(x)$ when one is close enough to the boundaries, so must create two minima there. We denote their positions 
$x_1^\star$ and $x_2^\star$, respectively, and that of the
intermediate maximum by $x_s$.
We also introduce
\BE
\Delta_0 = V(0) - V(x_s), &~&  \Delta_1 = V(1) - V(x_s),\nonumber \\
B_0 = V(0) - V(x^\star_1),&~& B_1 = V(1) - V(x^\star_2) \EE
as
illustrated in Fig.~\ref{fig:pot_ill}. 
As $v$ and $w$ change, so will the values of these barrier parameters. In particular, the signs of $\Delta_0$ and
$\Delta_1$ determine qualitatively the kind of fixation dynamics that the system will exhibit. The regime where $\Delta_0$ and $\Delta_1$ have {\em different} signs is subdivided further according to their relation to the barriers $B_0$ and $B_1$. A graphical summary is given in Fig.~\ref{fig:cases} and discussed further below. Fig.~\ref{fig:pg} shows the resulting phase diagram in the $(v,w)$-plane, and summarizes to what extent fixation probabilities and fixation times depend on initial conditions in each of the four regimes. Note that when $w$ gets too close to zero, or $|v|/|w|$ becomes too large, a maximum and a minimum of $V(x)$ can merge in a bifurcation. In the single minimum regime beyond this, the fixation dynamics becomes simple again and has the same features as for coexistence and dominance games. The arrow in Fig.~\ref{fig:pg} shows how the various regions of the diagram are traversed when $\lambda$ is increased at fixed $\tilde v$ and $\tilde w$, i.e.\ for fixed payoffs.
In Fig.~\ref{fig:coord_app} we plot over what $\lambda$-ranges $V(x)$ has the shapes (i), (iii) and (iv), respectively, in the specific example game of Sec.~\ref{sec:simple-examples-two}. The $\lambda$-range for shape (iii) is too small to see in that figure, however.

\begin{figure}[t!!!]
  \includegraphics[width=0.98\textwidth, left]{02_Chapters/PopulationOfIdeas/cases.pdf}
\caption[Schematic of the shape of the potential $V(x)$ in the four different classes of coordination games]{ Schematic of the shape of the potential $V(x)$ in the four different classes of coordination games. Arrows on the bottom of each panel represent deterministic relaxation paths that occur during fixation (full lines) as well as activated events driven by fluctuations (dashed lines). The legends indicate whether the large $N$-fixation probability and fixation time depend on the initial position $x$, or are uniform in $x$.
% {\bf (i)} When $\Delta_0 < 0$ and $\Delta_1 < 0$, the barriers to fixation at the boundaries are sma%ller than the central barrier separating the two potential minima $x_1^\star$ and $x_2^\star$: fixati%on occurs by deterministic relaxation to one of these points, then activation to the nearest boundary%.
%    {\bf (ii)} For $\Delta_0 >0$ and $\Delta_1 > 0$, transitions between the two potential minima are% much faster than activated fixation at either boundary. The system equilibrates between the minima, %forgetting its initial condition, and fixes at the boundary with lower $V(x)$, here $x=1$.
%    {\bf (iii,iv)} When $\Delta_0 < 0$ and $\Delta_1 > 0$, fixation always occurs at $x=0$ because fr%om $x^\star_2$ the system will cross the barrier at $x_s$ to $x^\star_1$. {\bf (iii)}  If the barrier% crossing is faster than the final activation time towards $x=0$, also the fixation time is independe%nt of the initial condition. {\bf (iv)} Otherwise, the barrier crossing time dominates, causing a muc%h longer fixation time when starting from $x>x_s$.
  }
    \label{fig:cases}
\end{figure}
 

Fig.~\ref{fig:cases}(a) shows the simplest case $\Delta_0,\Delta_1<0$. Here depending on its initial condition, the system will first relax to one of the minima of the potential, say $x^\star_1$. Then because $\Delta_0<0$ the barrier for activation to $x=0$ is smaller than for activation to the maximum $x_s$. For large $N$ -- which we always assume in the following discussion -- then with probability one the former process is the first to happen: fixation occurs at $x=0$. Similarly if the initial relaxation goes to $x^\star_2$ because the system started at $x>x_s$, fixation will occur at $x=1$. The fixation probability at $0$ is therefore a step function of the initial condition $x$, dropping from one to zero at $x=x_s$. The fixation time changes similarly with initial condition, from $\exp[N\Gamma B_0]$ for $x<x_s$ to $\exp[N\Gamma B_1]$ for $x>x_s$.

The opposite case of $\Delta_0,\Delta_1>0$ is illustrated in Fig.~\ref{fig:cases}(b). Here once the system has landed in either of the two minima, it will be able to reach the maximum separating these minima much faster than a boundary. As a result the system will make many ``trips'' between the two minima and effectively equilibrates across them, forgetting its initial condition. One can show that fixation will then eventually occur as if the system only had a single potential minimum at the {\em lower} of the two local potential minima, and will accordingly take place at the boundary with the lower value of $V$.


Finally there is the case where $\Delta_0$ and $\Delta_1$ have opposite signs, e.g.\ $\Delta_1>0$, $\Delta_0<0$ as shown in Fig.~\ref{fig:cases}(c,d). If the system starts out of $x<x_s$, we have the same case as (a) above: deterministic relaxation to $x^\star_1$ followed by fixation at $x=0$ on a timescale set by the barrier $B_0$. Otherwise, the system will initially relax to $x^\star_2$ and then traverse the maximum at $x_s$: $\Delta_1>0$ ensures that activation to the maximum is exponentially faster than fixation at $x=1$. After arrival at $x^\star_1$ the earlier sequence of processes is followed. Because fixation in both cases takes place at $x=0$, the fixation probability is independent of the initial condition. 

Whether the fixation time has such a dependence, on the other hand, depends on timescales. As Fig.~\ref{fig:cases}(c,d) shows, the timescale for activation from $x^\star_2$ to $x_s$ is set by the barrier $B_1-\Delta_1$, while the timescale for fixation at $x=0$ from $x^\star_1$ is set by $B_0$. If the former is smaller than the latter, as in Fig.~\ref{fig:cases}(c), then even when the system initially relaxes to $x^\star_2$, the timescale for the overall fixation trajectory will be given by $B_0$: it is therefore independent of the initial condition. In the converse case of Fig.~\ref{fig:cases}(d), the system will take longer to reach fixation starting from $x>x_s$ because activation from $x^\star_2$ to $x_s$ is much slower than fixation from $x^\star_1$. A typical fixation trajectory here will see the system spend almost all of its time near $x^\star_2$, before a fluctuation drives it across $x_s$ to $x_1^\star$ and from there to $x=0$. 

 

\begin{figure}[t!!]
  \centering
  \includegraphics[width=0.7\textwidth]{02_Chapters/PopulationOfIdeas/phidiag.pdf}
\caption{Phase diagram in the $(v,w)$-plane, indicating where the different shapes of $V(x)$ occur. These are explained in Fig.~\ref{fig:cases}.
  The dotted arrow shows how the phase diagram is traversed at fixed $\tilde v$ and $\tilde w$ when $\lambda$ is increased.
  }
  \label{fig:pg}
\end{figure}


\section{Fixation in regions of small flow}
\label{sec:fixat-regi-small}
Here, we explain briefly why the noise-driven escape from the low-flow region around an unstable fixed fixed point takes a time scaling as $\ln(N)$.

Consider the linearized dynamics of a coordination (or other) game near an unstable fixed point. After the mapping to Langevin dynamics with additive noise, cf.\ \eqref{eq:3}, this can be written in the form
\begin{equation}
  \label{eq:ldyn}
  \dot{y} = \tilde{\mu}(y - y_0) + \frac{1}{\sqrt{N}}\xi(t)
\end{equation}
with $\tilde{\mu}  >  0$. Assuming that $y(0)=y_0$, a straightforward calculation then shows that the variance of $y(t)$ is:
\begin{equation}
  \label{eq:ldynvar}
  \langle[y(t) - y_0]^2\rangle = \frac{\exp(2 \tilde{\mu} t)-1}{2 \tilde{\mu} N }
\end{equation}
To have `escape' from the unstable fixed point this needs to be of order unity; call this value $c$. Neglecting the $-1$ in the numerator then gives an escape time of order $t=\ln(2c \tilde\mu Nc+1)/(2\tilde \mu)$ which for large $N$ becomes $\ln(N)/(2\tilde \mu)$, establishing the promised logarithmic scaling with $N$. Note that while we have estimated the time for an escape to a distance of order unity in $y$-space, this is equivalent to an order unity distance in $x$-space as the mapping from $x$ to $y$ is smooth.

\end{subappendices}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: ../../MainFile.tex
%%% End:
