%%% File encoding is ISO-8859-1 (also known as Latin-1)
%%% You can use special characters just like �,� and �
\newpage
\chapter{Introduction}
\section{A really brief introduction to game theory}

Game theory aims at the ``study of mathematical models of conflicts and cooperation between intelligent, rational decision-makers''\cite{fvega}. 
Given such a broad definition, it is not surprising that game theory has\myMarginnote{Generalities}
many different fields of application. These range from the design of efficient market mechanisms~\cite{Milgrom2000} to biology where it is used to model
population dynamics~\cite{MaynardSmith1973,hofbauer,r.dawkins1976the-selfish-gen} and even the development of efficient policies~\cite{McCain2009} and the design of multi-agent systems~\cite{Wooldridge2002}.

% $ The learning dynamics problem
To give a brief introduction to \emph{Game theory} and to the concept of Nash equilibrium, let us consider the well-known \emph{prisoners' dilemma} where two bandits -- called \emph{players} in game theory -- are arrested and suspected of having committed a crime.
They are placed in two distinct interrogation rooms, cannot communicate and are faced with two choices: betraying their fellow or remaining silent.
If they both remain silent, they will both be imprisoned for one year, and if they betray each other, they will both go to prison for five years.
However, if one of them betrays the other while his fellow remains silent, the prisoner who has betrayed will not be convicted, and the silent prisoner will be sentenced to ten years in prison.
We can summarise this situation by the \emph{payoff matrix} shown in Tab.~\ref{tab:intro:PayoffMatrix}.
One question that arises is what strategies players are likely to play.

One possible approach to answering this question is to look for strategies corresponding to an ``equilibrium''.
We could, for example, consider the \emph{Nash equilibrium}~\cite{nash1950equilibrium} which generalises the notion of equilibrium -- that is common in physics -- to games such as the prisoners' dilemma.\myMarginnote{The Nash equilibrium}
Its  definition is: 
\emph{a strategy profile (\ie{} the list of probabilities for picking each of the available actions for each player) is said to be a Nash equilibrium if none of the players (here the bandits) can increase his payoff (here minus the number of years to be spent in jail) by changing strategy \emph{unilaterally}}.
One can also see the Nash equilibrium as the long run outcome of  a game repeatedly played when each player plays the best response to the former strategy profile (this is the \emph{best response dynamics}).
For example in our game, as shown in the Tab.~\ref{tab:intro:PayoffMatrix}, a silent bandit will always decrease his jail sentence if he betrays.
As a consequence, the only possible Nash equilibrium for the prisoners' dilemma is when both bandits betray.
The notion of a Nash equilibrium is one possibility among many to define the equilibrium of \emph{rational} players in games.
(Un)fortunately, in practice, humans are not rational.
They might be subject to biases~\cite{Kahneman2003,kahneman1974,Simon1955}, might not be capable of calculating their best strategy and end up not converging to their Nash equilibrium~\cite{Neyman1985}.
Thankfully, modeling \emph{bounded rationality} is an active area of research of which we want to give a brief taste in the next paragraph. 
\begin{table}[t!]
  \centering
  \includegraphics[scale=0.7]{02_Chapters/Intro/IntroPayoff}
  \caption[Payoff matrix of the prisoners' dilemma]{Payoff matrix of the prisoners' dilemma game where each line corresponds to one strategy available for the first bandit and each column to one strategy available to the second bandit.
    The entries of the matrix are of the form (payoff of bandit 1, payoff of bandit 2) where the payoff of a bandit is minus the number of years he will spend in jail.
    The arrows (red for bandit one and black for bandit 2) show how a bandit would change his strategy under the best response dynamics.
    We see that the Nash equilibrium (Betray, Betray) is the only strategy profile that is stable under the best response dynamics.}
  \label{tab:intro:PayoffMatrix}
\end{table}
% \todo[inline]{bibliography to be continued}
%One possible direction to do so is to consider the learning dynamics of players.

One possible way to take into account bounded rationality is to consider players who do not follow best-response dynamics.
For an extensive review of learning dynamics in games, we refer to~\cite{fudenberg1998theory}. 
Among the existing learning dynamics in games, \emph{belief-based learning} where traders learn their beliefs about the probable actions taken by other players and \emph{reinforcement-based learning} where players reinforce their strategies based on the payoff they provide are some of the most interesting.
In this thesis, we focus specifically in \emph{Experience-Weighted Attraction}(EWA) learning, which is an interpolation between belief based learning and reinforcement based learning~\cite{Camerer1999}. 

% \todo[inline]{Shall I write a short introduction of EWA learning dynamics}
Another direction to go beyond the full rationality assumption is to consider evolutionary games, which model Darwinian selection. \myMarginnote{evolutionary game theory}
In evolutionary games, one no longer studies players who learn to play against each other, but instead populations of individuals that reproduce proportionally to how well-suited they are to the environment.
The approach is rooted in the seminal paper of Maynard-Smith \etal{}~\cite{MaynardSmith1973} ``The logic of animal conflict''.
% One interesting outcome of evolutionary game theory is the notion of an \emph{evolutionary stable strategy} (ESS), which is the counterpart of a Nash equilibrium for evolutionary games. 
We refer the interested reader to an extensive review of evolutionary game theory in the textbook of Hofbauer and Sigmund~\cite{hofbauer}. 

\section{Mission statement} 
In brief, the aim of this thesis is to study the long run outcome of different learning dynamics in games using tools coming from statistical physics as well as large deviation methods.
Such an approach makes it possible to obtain the long run outcome of EWA learning dynamic in limiting cases (players with large memory) and to calculate finite size quantities such as first passage times.
More specifically we investigate rare events in learning dynamics and their impact on the shape of the distribution of strategies among players.\myMarginnote{motivation for the thesis}
Our particular motivation is to study \emph{segregation}, a phenomenon whereby players spontaneously split into groups adopting different strategies. We pursue this question in the context of players choosing between multiple double auction markets, a setup that we model as an \emph{aggregative game}~\cite{AggregativesCorchon} with a \emph{large number of players}.
Aggregative game here designates a game where the payoff of players only depends on their own strategy and some mean field quantities. These summarise the behaviour of the other players and are called aggregates.
For example, in this thesis where we study segregation among several markets, the aggregates are the number of buyers and the number of sellers in each market.
Another famous example of an aggregative game is the Cournot model of oligopoly~\cite{cournotBook}.
This model is concerned with several firms who compete on the amount of a good they produce.
In this model, the payoff of firm $i$ is $q_i P(\bar{q}) - C(q_i)$ where $C(q_i)$ is the cost of production of $q_i$ units of good and $P(\bar{q})$ is the price of a single unit of good. The price of a single unit of good depends on the total quantity of a good produced $\bar{q} = \sum_i q_i$ which is the aggregate we mentioned earlier. 
Since the payoff of the agents depends only on the aggregates of the agents, there can be many distributions of strategies among the players of an aggregative game that are Nash-equilibria.
There is a vast literature on the dynamics of aggregates when players learn to play an aggregative game~\cite{kash2011multiagent,friedman1997learning}.
However, to assess the existence of groups, one needs to know the structure of the distribution of strategies in the population of traders and not only the aggregates quantities. 
There are no previous studies of this structure in large aggregative games we are aware of. 
%That said, we did not find any studies which investigate the structural properties of the strategies distributions in a large aggregative.
The aim of this thesis is twofold: (i) to investigate the structure of the steady state of some learning dynamics as well as the finite size effects such as first passage times, (ii) to apply those results to help understand the spontaneous emergence of segregation of traders across double auction markets.




Having stated the motivation for this thesis, we give an overview of the methods -- mostly coming from statistical physics -- that we will use in this thesis. 
The application of methods from physics to game theory proved to be successful in the past as attested by a substantial body of literature.
For example, a number of papers~\cite{bladon2010evolutionary,altrock,traulsen2009stochastic,antal2006fixation} are concerned with finite size effects in birth-death processes in evolutionary game theory.
Other studies~\cite{Sato2005,Sato2003,satofarmer,galla2013complex,pangallo2017taxonomy} are concerned with learning dynamics in games.
One can also mention~\cite{DallAsta2012} where Dall'Asta \etal{} study the ``Collaborative Nash equilibria'', of public good games on networks.
The generalization to random graphs of their model is performed with the well-known cavity method~\cite{9971501155}.
On top of these works, some recent contributions to mean field game theory originated from statistical physicist~\cite{Swiecicki2016,Swiecicki2015}.

The minority game~\cite{minoritygamebookCoolen,minoritygamebook} is also an active area of research in the physics community.
As its name suggests, it is a model where players compete to be in the minority.
An example of a minority game is ``which road to choose to avoid the traffic jam on the way home, knowing that all your colleagues will face the same choice''.
This means choosing the road the \emph{minority} of your colleagues choose to use.
It originates from the El-Farol Bar problem~\cite{elfarolbar} and has received attention from physicists because of its formal relation to spin glasses models.
For a non-exhaustive literature review on minority games, we refer to the articles collected in the second part of~\cite{minoritygamebook}. 
Two papers~\cite{Huang2012,PhysRevLett.82.3360} are relevant here as they are concerned with segregation in minority games.
In~\cite{Huang2012} agents who play a multi-resources minority game are distributed on a graph and only observe their neighbour, whose winning strategy they copy. In such models Huang \etal{} observe a strategy grouping behaviour.
The major difference between the model of Huang \etal{} and our model is that the locality of information they assume, which fairly directly leads to segregation, is not a feature of model of segregation in double auction markets. 
The authors of~\cite{PhysRevLett.82.3360} study \emph{evolutionary minority games}.
In such a game, agents have a common memory of the output of previous sessions of the minority game and choose to play either according to it with probability $p$ or against it with probability $1-p$.
The agents learn the strategy $p$ and in the long run, a strategy grouping is observed.

In this thesis we will frequently use the \KM{} expansion~\cite{vankampen2007spp}, Kramers' reaction rate theory~\cite{vankampen2007spp,hanggi1990reaction}, mean field theory and also the Freidlin-Wentzell theory.\myMarginnote{methods}
Freidlin-Wentzell generallises Kramers' rate theory to non conservative dynamics~\cite{bouchetetal,fwtheory}.
The main book about Freidlin-Wentzell theory being hardly readable for a physicist, we suggest to the interested reader the paper of Bouchet \etal{}~\cite{bouchetetal}, which has a very clear introduction to Freidlin-Wentzell theory. 
The most challenging problem we encountered when using the Freidlin-Wentzell theory is to calculate transition rates between two stable points of a dynamics. This requires finding the minimal action path between those two points. There is a variety of algorithms for this task, two of which are explained in~\cite{bunin2012large,heymann2008pathways}.  
The ultimate goal of the Freidlin-Wentzell theory is to provide a picture of the behaviour of complex systems in the low noise limit.
Such a low noise limit is a recurring theme all along this thesis either because the noise comes from the large size of the system (see Chap.~\ref{chap:popideas}) or from the large memory of traders in EWA learning dynamics (see Chap.~\ref{chap:Dynamic},  \ref{chap:3M} and~\ref{chap:HetMem}).
As usual in statistical physics in the low noise limit, the problem structure is simplified and often allows for an analytical solution.
In the next subsection, we describe the structure of the thesis.

\section{Organisation of the thesis}
In this PhD thesis, we study different learning dynamics in the low noise limit. The connection between the chapters is shown graphically in Fig.~\ref{fig:roadmap}.
In Chap.~\ref{chap:Dynamic} we study the learning dynamics of traders choosing among double auction markets.
We first look at the equilibrium of this game in the ``Nash sense''. Then we compare this Nash equilibrium to the steady state of Experience-Weighted Attraction learning dynamics with fictitious play.
We find that in the limit of full fictitious play, large memory and large intensity of choice,
the steady states of the learning dynamics do correspond to Nash equilibria.
One of the surprising outcomes of our research is that depending on how the relevant limit of Experience-Weighted Attraction learning is approached, the learning dynamics has qualitatively different long-run outcomes.
Each corresponds to a Nash equilibrium, and some have groups of players playing with mixed strategies, a scenario not considered before.

In Chap.~\ref{chap:Dynamic} we focus for simplicity on traders choosing between only two markets. 
Chap.~\ref{chap:3M} extends our study to the case of three markets. This connects to a preliminary simulation study of this setting by Alori\'c~\cite{thesisaleksandra} and puts the results on a firmer theoretical footing thanks to the minimal action path formalism developed in Chap.~\ref{chap:Dynamic}. This enables us to investigate the existence of strong segregation, where ``strong'' refers to segregation that persists in the large memory limit.

In real markets, traders have different objectives, depending for example on whether they are speculators or investors. They can have different risk aversion, want to make a profit on different timescales (days, months) etc. 
To take into account such heterogeneity we study
in Chap.~\ref{chap:HetMem} a model where the population of traders has heterogeneity in both their memory length and their intensity of choice. 
In our model, the forgetting rate of traders $r$ represents the inverse time scale over which agents aim to make a profit. We consider the existence of segregation or otherwise in a population where some traders have an inverse memory $r = 1$ while the rest has a small inverse memory $r \ll 1$. We also study additional heterogeneity in the decision strength $\beta$, which is the extent to which traders act on preferences learnt from previously received payoffs.


In Chap.~\ref{chap:popideas} we give an interpretation of reinforcement learning as a pairwise matching process in populations of ideas.
This analogy is, on the one hand,  motivated by existing work~\cite{Sato2003,Sato2005,Tuyls2003,tuyls2006evolutionary,Keisers2012} linking reinforcement learning to the replicator equation of evolutionary game theory, with a modified fitness that includes an entropic term. The resulting equation is known as the \SC equation.  
On the other hand, there is a large litterature~\cite{traulsen2005coevolutionary,traulsen2009stochastic,traulsen2005coevolutionary,mobilia2010fixation} that investigates how one can interpret the original replicator equation as the large size limit of a pairwise matching process and what properties -- including e.g.\ fixation times -- this process has for finite population size.
Keeping this fact in mind, we show that also the \SC equation can be written as a pairwise matching process, where the fitness of agents strategies contain an entropic term that accounts for the willingness of players to randomise their strategies, \ie{} how they set their exploration-exploitation trade-off~\cite{RLIntro}.
We describe birth-death processes in a population of ideas in the limit of a large population as a sequence of simple events (\emph{relaxation} which last a time of order 1, \emph{activation} which takes a time exponential in the size of the population $N$, ...) which provides a useful intuitive framework for understanding the dynamics of ideas in the large size limit.

Finally, in Chap.~\ref{chap:Conclusion} we summarise the results of our work and discuss some possible applications as well as future research directions.
\begin{figure}
\input{02_Chapters/Intro/PlanThesis.tikz}
\caption[Roadmap through the thesis]{\textbf{Roadmap through the thesis:}
  Chap.~\ref{chap:Dynamic} is self-contained and describes, among other things, the large deviation formalism used throughout this thesis. Chaps.~\ref{chap:3M} and~\ref{chap:HetMem} rely on the methods developed in Chap.~\ref{chap:Dynamic} and deal with two extensions of the model of segregation in double auction markets.
  Chap.~\ref{chap:popideas} is a self-contained study of rare events such as fixation in a \emph{large} population of ideas that follows a birth death process.
}
\label{fig:roadmap}
\end{figure}
% \Blindtext[3][1]
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../MainFile"
%%% End:
