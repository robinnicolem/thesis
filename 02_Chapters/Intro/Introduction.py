#!/usr/gin/python
# Generate the figure for the introduction
import pylab as plt
# Preamble
plt.rc('text', usetex=True)
plt.rc('font', family='serif')
plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.size'] = 18
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 18
plt.rcParams['ytick.labelsize'] = 18
plt.rcParams['legend.fontsize'] = 16
plt.rcParams['legend.numpoints'] = 1
# Plotting the figrue
SpLv = 'Small profit \n Large volume'
LpSv = 'Large Profit \n Small volume'
plt.plot([0.5, 0.5], [0, 1], c='black')
plt.plot([0, 1], [0.5, 0.5], c='black')
plt.text(0.1, 0.7, SpLv, fontsize=20, color='blue')
plt.text(0.6, 0.2, SpLv, fontsize=20, color='blue')
plt.text(0.1, 0.2, LpSv,  fontsize=20, color='red')
plt.text(0.6, 0.7, LpSv, fontsize=20, color='red')
plt.xlabel('$\\theta$',  fontsize=20)
plt.ylabel('$p_{\\rm b}$', fontsize=20)
plt.xticks([0, 0.5, 1])
plt.yticks([0, 0.5, 1])
plt.scatter(0.3, 0.2)
plt.scatter(0.3, 0.8)
plt.show()
