all: 
	make compile
	make diff-last-correction 
	make diff-examiners-corrections
compile:
	-pdflatex -synctex=1 MainFile.tex > /dev/null
	-bibtex Mainfile > /dev/null
	-pdflatex -synctex=1 -shell-escape -interaction=nonstopmode MainFile.tex > /dev/null
	-bibtex Mainfile > /dev/null
	-pdflatex -synctex=1 -shell-escape -interaction=nonstopmode MainFile.tex > /dev/null
	-bibtex Mainfile > /dev/null
	-pdflatex -synctex=1 -shell-escape -interaction=nonstopmode MainFile.tex > /dev/null

diff-last-correction:
	-git latexdiff --ignore-makefile -o diff_last_corrections.pdf --ignore-latex-errors --main MainFile.tex d4b73d8b653a1d3d3c04947ca522a817c8350c07 --  

diff-examiners-corrections:
	-git latexdiff --ignore-makefile -o diff_complete.pdf --ignore-latex-errors --main MainFile.tex 572260c7ff0d42d4fb8b17e825848c62a9cbae32 --
